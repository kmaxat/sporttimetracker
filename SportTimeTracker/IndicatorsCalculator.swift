//
//  IndicatorsCalculator.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 21.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import Foundation

enum unitSpeed: Double {
    case km = 3.6
    case mi = 2.236936
}

enum unitDistance: Double {
    case km = 1000
    case mi = 1600
}

class IndicatorsCalculator {
    
    var distance = Double()
    var speed = Double()
    var seconds = 0
    var time = 0.0
    var currentUnit = unitUser.km
    
    init() {
        var unit =  keychain.get("unit")
        if unit == nil {
            unit = "km"
        }
        switch unit! {
            case "mi":
                currentUnit = unitUser.mi
            default:
                currentUnit = unitUser.km
            }
    }
    
    var locations = [CLLocation]()
    
    func calculateIndicators() {
        if locations.count > 0 {
            if(locations.count-2 > -1){
                distance = distance + locations[locations.count-2].distance(from: locations.last!)
                let l1 = locations.last!
                let l2 = locations[locations.count-2]
                let distance2 = l2.distance(from: l1)
                let time2 = l1.timestamp.timeIntervalSince(l2.timestamp)
                speed = distance2/time2
            } else {
                speed = 0
            }
            time = Double(seconds) / 3600
          
        }
    }
    
    func _distance(inputDistance: Double? = nil) -> Double {
        let distanceCont = inputDistance ?? self.distance

        var distance: Double
        switch currentUnit {
        case .km:
            distance = distanceCont / unitDistance.km.rawValue
        case .mi:
            distance = distanceCont / unitDistance.mi.rawValue
        }
        return distance.isNaN == false ? distance : 0.0
    }
    
    func _speed() -> Double {
        var speed: Double
        switch currentUnit {
        case .km:
            speed = self.speed * unitSpeed.km.rawValue
        case .mi:
            speed = self.speed * unitSpeed.mi.rawValue
        }
        return speed.isNaN == false ? speed : 0.0
    }
    
    func _avgSpeed() -> Double {
        var avgSpeed: Double
        switch currentUnit {
        case .km:
            avgSpeed = (distance / time) / unitDistance.km.rawValue
        case .mi:
            avgSpeed = (distance / time) / unitDistance.mi.rawValue
        }
        return avgSpeed.isNaN == false ? avgSpeed : 0.0
    }
    
    func _pace() -> Double {
        if(_distance() > 0) {
            let pace = Double(seconds) / _distance()
            return pace.isNaN == false ? pace : 0.0
        } else {
            return 0.0
        }
}
    
    func _avgPace() -> Double {
        let paceSec = Double(seconds) / _distance()
        let paceMin = floor(paceSec.remainder(dividingBy: 60))
        let avgPace = paceSec - paceMin
        return avgPace.isNaN == false ? avgPace : 0.0
    }
}
