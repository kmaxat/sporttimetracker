//
//  Converter.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 19/10/2016.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

class Converter {

    class func secondsToMilisec(sec: Double) -> Double {
        return sec * 1000
    }
    
}
