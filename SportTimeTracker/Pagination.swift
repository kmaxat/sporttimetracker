//
//  Pagination.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper

class Meta: Mappable {
    
    var pagination: Pagination?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        pagination <- map["pagination"]
    }

    
}

class Pagination: Mappable {

    var current_page: Int = 0
    
    var total_pages: Int = 0
    
    var per_page: Int = 0
    
    var count: Int = 0
    
    var links: [String]?
    
    var total: Int = 0
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        current_page <- map["current_page"]
        total_pages <- map["total_pages"]
        per_page <- map["per_page"]
        count <- map["count"]
        links <- map["links"]
        total <- map["total"]
    }
    
}
