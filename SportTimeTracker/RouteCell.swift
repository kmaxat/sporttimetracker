//
//  RouteCell.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 22.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

class RouteCell: UITableViewCell {

    @IBOutlet weak var imgRoute: UIImageView!
    @IBOutlet weak var txtSportTitle: UILabel!
    @IBOutlet weak var txtDistance: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
