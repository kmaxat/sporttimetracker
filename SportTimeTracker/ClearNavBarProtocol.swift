//
//  ClearNavBarProtocol.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 09.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol ClearNavBarProtocol {

    func clearNavBar(_ nav: UINavigationController)
    
}

extension ClearNavBarProtocol {
    func clearNavBar(_ nav: UINavigationController) {
        nav.isNavigationBarHidden = false
        nav.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        nav.navigationBar.shadowImage = UIImage()
        nav.navigationBar.isTranslucent = true
        nav.view.backgroundColor = UIColor.clear
    }
}
