//
//  SelectIndicatorVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 19.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol ChooseIndicatorDelegate {
    func didiChooseIndicator(_ indicator: Indicator, name: String, indicatorPlace: Int)
}

class SelectIndicatorVC: UITableViewController {
    
    var items = [String]()
    var images = [String]()
    
    var delegate: ChooseIndicatorDelegate?
    var indicatorPlace: Int?
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        items = [
            "Дистанция",
            "Длительность",
            "Скорость",
            "Темп",
            "Средняя скорость",
            "Средний темп"
            ]
        
        images = [
            "distance_icon",
            "time",
            "speed",
            "pace_icon",
            "avg_speed_icon",
            "avg_pace_icon"
        ]
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                                               for: indexPath)
        cell.textLabel?.text = items[(indexPath as NSIndexPath).row]
        cell.imageView?.image = UIImage(named: images[indexPath.row])?.resizeImageWithAspect(scaledToMaxWidth: 20, maxHeight: 20)
        cell.imageView?.contentMode = .scaleAspectFit
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath as NSIndexPath).row {
        case 0:
            self.delegate?.didiChooseIndicator(.distance, name: self.items[(indexPath as NSIndexPath).row], indicatorPlace: self.indicatorPlace!)
        case 1:
            self.delegate?.didiChooseIndicator(.time, name: self.items[(indexPath as NSIndexPath).row], indicatorPlace: self.indicatorPlace!)
        case 2:
            self.delegate?.didiChooseIndicator(.speed, name: self.items[(indexPath as NSIndexPath).row], indicatorPlace: self.indicatorPlace!)
        case 3:
            self.delegate?.didiChooseIndicator(.pace, name: self.items[(indexPath as NSIndexPath).row], indicatorPlace: self.indicatorPlace!)
        case 4:
            self.delegate?.didiChooseIndicator(.avgSpeed, name: self.items[(indexPath as NSIndexPath).row], indicatorPlace: self.indicatorPlace!)
        case 5:
            self.delegate?.didiChooseIndicator(.avgPace, name: self.items[(indexPath as NSIndexPath).row], indicatorPlace: self.indicatorPlace!)
        default: break
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
}
