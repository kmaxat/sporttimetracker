//
//  User.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper

enum friendStatus: String {
    case none = ""
    case accepted = "accepted"
    case requested = "requested"
}

enum unitUser: String, RawRepresentable {
    case km
    case mi
}

class User: Mappable {
    
    var height: Int = 0
    
    var weight: Double = 0
    
    var id: Int = 0
    
    var gender = String()
    
    var friend: friendStatus = friendStatus.none
    
    var last_name = String()
    
    var birthday = String()
    
    var email = String()
    
    var picture: String?
    
    var first_name = String()
    
    var verified = Bool()
    
    var trainings_count: Int = 0
    
    var friends_count: Int = 0
    
    var onesignal_id = String()
    
    var unit = unitUser.km
    
    init () {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        gender <- map["gender"]
        height <- map["height"]
        weight <- map["weight"]
        id <- map["id"]
        last_name <- map["last_name"]
        birthday <- map["birthday"]
        email <- map["email"]
        picture <- map["picture"]
        first_name <- map["first_name"]
        onesignal_id <- map["onesignal_id"]
        verified <- map["verified"]
        trainings_count <- map["trainings_count"]
        friends_count <- map["friends_count"]
        unit <- map["unit"]
        friend <- (map["friend"],EnumTransform<friendStatus>())
    }
}


class EmailConfirmed: Mappable {
    
    var message: String?
    var status_code: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        status_code <- map["status_code"]
    }
}
