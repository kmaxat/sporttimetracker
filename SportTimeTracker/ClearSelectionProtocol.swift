//
//  ClearSelectionProtocol.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol ClearSelectionProtocol {
    func clearTC(_ tc: UITableViewController)
}

extension ClearSelectionProtocol {
    func clearTC(_ tc: UITableViewController) {
        if tc.tableView.indexPathForSelectedRow != nil {
            tc.tableView.deselectRow(at: tc.tableView.indexPathForSelectedRow!, animated: true)
        }
    }
}
