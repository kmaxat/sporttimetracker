//
//  UserProfileVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 19.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import Nuke
import PKHUD

class UserProfileVC: UIViewController {

    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtTrainingsCount: UILabel!
    @IBOutlet weak var txtFriendsCount: UILabel!
    @IBOutlet weak var friendshipBtn: UIButton!
    var userID: Int?
    var hideCloseBtn = Bool()
    var user: User!
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUser()
        binds()
        
        self.closeBtn.isHidden = hideCloseBtn
    }
    
    func binds() {
        closeBtn.rx.tap.bindNext {
            self.dismiss(animated: true, completion: nil)
            }.addDisposableTo(bag)
        
        friendshipBtn?.rx.tap
            .bindNext({
                if self.user.friend == .none {
                    self.addFriend()
                } else {
                    self.removeFriend()
                }
            }).addDisposableTo(bag)
    }
    
    func getUser() {
        HUD.show(.progress)
        Network.request(target: .getUserWithID(id: self.userID!))
            .mapObject(User.self)
            .subscribe(onNext: { (user) in
                self.user = user
                if let url = URL(string: user.picture ?? "") {
                    Nuke.loadImage(with: url, into: self.avatarImg)
                }else {
                    self.avatarImg.image = #imageLiteral(resourceName: "avatar_placeholder")
                }
                self.txtName.text = user.first_name + " " + user.last_name
                self.txtTrainingsCount.text = "\(user.trainings_count)"
                self.txtFriendsCount.text = "\(user.friends_count)"
                print(user.friend.rawValue)
                if user.friend == .accepted {
                    self.friendshipBtn.setTitle("В друзьях", for: .normal)
                } else if user.friend == .requested {
                    self.friendshipBtn.setTitle("Запрос отправлен", for: .normal)
                    self.friendshipBtn.isUserInteractionEnabled = false
                }
                }, onError: { (error) in
                    HUD.hide()
                }, onCompleted: {
                    HUD.hide()
                }, onDisposed: nil).addDisposableTo(bag)
    }
    
    func addFriend() {
        HUD.show(.progress)
        Network.request(target: .addFriend(id: self.userID!, decision: "accepted"))
            .subscribe(onNext: { (response) in
                if response.statusCode < 300 {
                    showAlert("SportTime", message: "Приглашение отправлено", controllder: self)
                } else {
                    showErrorAlertWithCode(code: response.statusCode, controller: self)
                }
                }, onError: { (error) in
                    HUD.hide()
                    showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                }, onCompleted: { 
                    HUD.hide()
                }, onDisposed: nil).addDisposableTo(self.bag)
    }
    
    func removeFriend() {
        let actionSheetController: UIAlertController = UIAlertController(title: "SportTime", message: "Убрать из друзей?" , preferredStyle: .alert)
        let nextAction: UIAlertAction = UIAlertAction(title: "Убрать", style: .destructive) { action -> Void in
            HUD.show(.progress)
            Network.request(target: .addFriend(id: self.userID!, decision: "remove"))
                .subscribe(onNext: { (response) in
                    if response.statusCode < 300 {
                        showAlert("SportTime", message: "Пользователь удален из друзей", controllder: self)
                    } else {
                        showErrorAlertWithCode(code: response.statusCode, controller: self)
                    }
                    }, onError: { (error) in
                        HUD.hide()
                        showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                    }, onCompleted: { 
                        HUD.hide()
                    }, onDisposed: nil).addDisposableTo(self.bag)
        }
        actionSheetController.addAction(nextAction)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Отмена", style: .cancel) { action -> Void in}
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
}
