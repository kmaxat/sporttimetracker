//
//  DistanceCalculator.m
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 28.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

#import "DistanceCalculator.h"

@implementation DistanceCalculator

- (BOOL)isCoordinate:(CLLocationCoordinate2D)coordinate closeToPolyline:(MKPolyline *)polyline {
    CLLocationCoordinate2D polylineCoordinates[polyline.pointCount];
    [polyline getCoordinates:polylineCoordinates range:NSMakeRange(0, polyline.pointCount)];
    
    for (int i = 0; i < polyline.pointCount - 1; i++) {
        CLLocationCoordinate2D a = polylineCoordinates[i];
        CLLocationCoordinate2D b = polylineCoordinates[i + 1];
        
        double distance = [self distanceToPoint:MKMapPointForCoordinate(coordinate) fromLineSegmentBetween:MKMapPointForCoordinate(a) and:MKMapPointForCoordinate(b)];
        if (distance < 25) {
            return YES;
        }
    }
    
    return NO;
}

- (double)distanceToPoint:(MKMapPoint)p fromLineSegmentBetween:(MKMapPoint)l1 and:(MKMapPoint)l2 {
    double A = p.x - l1.x;
    double B = p.y - l1.y;
    double C = l2.x - l1.x;
    double D = l2.y - l1.y;
    
    double dot = A * C + B * D;
    double len_sq = C * C + D * D;
    double param = dot / len_sq;
    
    double xx, yy;
    
    if (param < 0 || (l1.x == l2.x && l1.y == l2.y)) {
        xx = l1.x;
        yy = l1.y;
    }
    else if (param > 1) {
        xx = l2.x;
        yy = l2.y;
    }
    else {
        xx = l1.x + param * C;
        yy = l1.y + param * D;
    }
    
    return MKMetersBetweenMapPoints(p, MKMapPointMake(xx, yy));
}

@end
