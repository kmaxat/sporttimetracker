//
//  FriendsSearch.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 24.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import PKHUD
import Nuke

class FriendsSearch: UITableViewController, UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    
    var data = [DataFriends]()
    var searchController: UISearchController!
    var rootVC: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.data = []
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                                               for: indexPath) as! UserCell
        let user = data[indexPath.row]
        cell.txt.text = user.first_name + " " + user.last_name
        cell.img.image = nil
        if let url = URL(string: user.picture ?? "") {
           Nuke.loadImage(with: url, into: cell.img)
        }else {
            cell.img.image = #imageLiteral(resourceName: "avatar_placeholder")
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profileVC = st.instantiateViewController(withIdentifier: "User") as! UserProfileVC
        profileVC.userID = self.data[(indexPath as NSIndexPath).row].id
        self.present(profileVC, animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let search = searchController.searchBar.text {
            findFriends(searchText: search)
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if !searchController.isActive {
            return
        }
    }
    
    func findFriends(searchText: String) {
        HUD.show(.progress)
        Network.request(target: .getUsersSearch(q: searchText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!))
        .mapObject(Friends.self)
        .subscribe(onNext: { (friends) in
            self.data = friends.data ?? []
            }, onError: { (error) in
                HUD.hide()
                showErrorAlertWithCode(code: error.localizedDescription, controller: self)
            }, onCompleted: {
                HUD.hide()
                self.tableView.reloadData()
            }, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
