//
//  Trainings.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper
import MapKit
import Realm
import RealmSwift

class Hystorys: Mappable {

    var meta: Meta?

    var data: [DataHystory]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        meta <- map["meta"]
        data <- map["data"]
    }
}


class DataHystory: Object, Mappable {

    dynamic var id: Int = 0

    dynamic var average_speed: Double = 0

    dynamic var calories: Int = 0

    dynamic var max_speed: Double = 0

    dynamic var user_id: Int = 0

    dynamic var average_pace: Double = 0

    dynamic var notes = String()

    dynamic var date = String()
    
    fileprivate var route: [LocationObject] {
        set{
            routeList.removeAll()
            routeList.append(objectsIn: newValue)
        }
        get{
            return Array(routeList)
        }
    }
    
    let routeList = List<LocationObject>()

    dynamic var duration: Int = 0

    dynamic var sport: dataSport?

    dynamic var distance: Double = 0

    dynamic var max_pace: Double = 0
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        average_speed <- map["average_speed"]
        calories <- map["calories"]
        max_speed <- map["max_speed"]
        user_id <- map["user_id"]
        average_pace <- map["average_pace"]
        notes <- map["notes"]
        date <- map["date"]
        route <- map["route"]
        duration <- map["duration"]
        sport <- map["sport"]
        distance <- map["distance"]
        max_pace <- map["max_pace"]
    }
}

