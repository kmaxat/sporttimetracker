//
//  Api.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 13.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import ObjectMapper

class Network {
    fileprivate static var provider = RxMoyaProvider<SportTimeAPI>(endpointClosure :
        { (target: SportTimeAPI) -> Endpoint<SportTimeAPI> in
            let url = target.url
            let endpoint: Endpoint<SportTimeAPI> = Endpoint<SportTimeAPI>(URL: target.url, sampleResponseClosure: {.networkResponse(200, target.sampleData)}, method: target.method, parameters: target.parameters, parameterEncoding: target.parameterEncoding, httpHeaderFields: target.headers)
            return endpoint
    })
    
    class func request(target: SportTimeAPI) -> Observable<Response> {
        print(target.url)
        print(target.parameters ?? "")
        print(target.method)
        return Network.provider.request(target)
    }
}
