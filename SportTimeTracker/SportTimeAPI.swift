//
//  SportTimeAPI.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 13.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
import Moya_ObjectMapper
import KeychainSwift

private extension String {
    var URLEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }
}

enum SportTimeAPI {
    case loginUser(email: String, password: String)
    case registerUser(first_name: String, last_name: String, email: String, password: String, birth: String, gender: String)
    case confirmEmail(email: String)
    case didConfirmEmail(email: String)
    case loginFB(token: String)
    case loginGE(token: String)
    case forgotPass(email: String)
    
    case getSports()
    
    case inbox()
    case removeInbox(id: Int)
    
    case getUserMe()
    case postUserMe(first_name: String?, last_name: String?, email: String?, birth: String?, gender: String?, weight: String?, height: String?,
        unit: String?, onesignal_id: String?)
    case changePassword(oldPassword: String, newPassword: String)
    case getUserWithID(id: Int)
    case getUsersSearch(q: String)
    case postUserAvatar(avatar: UIImage)
    
    case activities()
    case getFriends()
    case addFriend(id: Int, decision: String)
    
    case getGroupTrainings(location: LocationObject, date: String)
    case getMyGroupTrainings()
    case getGroupTrainingWithIID(id: Int)
    case deleteGroupTraining(id: Int)
    case addGroupTraining(sport_id: Int, title: String?, location: LocationObject?, private_state: String?, start_date: String?, start_time: String?, description: String?, repeat_type: String?, invites_user: [Int]?, repeat_days: [Int]?)
    case putGroupTraining(training_id: Int, sport_id: Int?, title: String?, location: LocationObject?, private_state: String?, start_date: String?, start_time: String?, description: String?, repeat_type: String?, invites_user: [Int]?, repeat_days: [Int]?)
    case groupTrainingDecision(id: Int, decision: String)
    case groupTrainingUsersList(id: Int, type: String)
    
    case getMyTrainings()
    case getTrainingWithID(id: Int)
    case addMyTraining(route: [LocationObject], calories: Double, sport_id: Int, distance: Double, duration: Int, average_speed: Double, average_pace: Double, max_speed: Double, max_pace: Double, notes: String)
    case deleteMytraining(id: Int)
    
    case getMyRoutes()
    case getRoutes()
    case getRouteWithID(id: Int)
    
    case postMyRoute(title: String, sport_id: Int, private_state: String, distance: Double, center: LocationObject, route: [LocationObject])
    case postRouteTime(id: Int, sport_id: Int, duration_seconds: Int)
    
    case getAllRecordsForRoute(id: Int)
    
    case deleteRoute(id: Int)
    
    case getSettingsMe()
    case putSettingsMe(invitation_notifications: Bool?, friend_request_notifications: Bool?)
}

extension SportTimeAPI: TargetType {
    
    var parameterEncoding: Moya.ParameterEncoding {
        return method == .get ? URLEncoding() : JSONEncoding()
    }
    
    public var task: Task {
        switch self {
        case .postUserAvatar(let avatar):
            let imageData = UIImageJPEGRepresentation(avatar, 5)
            return .upload(.multipart([MultipartFormData(provider: .data(imageData!), name: "picture", fileName: "picture.jpg", mimeType:"image/jpeg")]))
        default:
            return .request
        }
    }

    var baseURL: URL {
        return URL(string: "https://sporttimeapp.com:4000")!
    }
    
    var path: String {
        switch self {
        case .inbox:
            return "/api/v1/inbox/me"
        case .removeInbox(let id):
            return "/api/v1/inbox/\(id)"
        case .loginUser:
            return "/api/v1/auth/login-email"
        case .registerUser:
            return "/api/v1/auth/register"
        case .confirmEmail:
            return "/api/v1/users/me/email"
        case .didConfirmEmail:
            return "/api/v1/users/me/email"
        case .activities:
            return "/api/v1/activities/me/?limit=100"
        case .loginFB(let token):
            return "/api/v1/auth/login-facebook?token=\(token)"
        case .loginGE(let token):
            return "/api/v1/auth/login-google?token=\(token)"
        case .getFriends:
            return "/api/v1/friends/me/?limit=100"
        case .addFriend(let id, _):
            return "/api/v1/friends/\(id)"
        case .getUserMe:
            return "/api/v1/users/me"
        case .postUserMe:
            return "/api/v1/users/me"
        case .postUserAvatar:
            return "/api/v1/users/me"
        case .changePassword:
            return "/api/v1/users/me/password"
        case .getUserWithID(let id):
            return "/api/v1/users/\(id)"
        case .getUsersSearch(let searchString):
            return "/api/v1/users/search?q=\(searchString)&limit=100"
        case .getGroupTrainings(let location, let date):
            return "/api/v1/grouptrainings?lat=\(location.lat)&lon=\(location.lon)&limit=100&date=\(date)"
        case .getMyGroupTrainings:
            return "/api/v1/grouptrainings/me/?limit=100"
        case .getGroupTrainingWithIID(let id):
            return "/api/v1/grouptrainings/\(id)"
        case .deleteGroupTraining(let id):
            return "/api/v1/grouptrainings/\(id)"
        case .addGroupTraining:
            return "/api/v1/grouptrainings/me"
        case .groupTrainingDecision(let id, _):
            return "/api/v1/grouptrainings/\(id)/decision"
        case .getSports:
            return "/api/v1/sports"
        case .getMyTrainings:
            return "/api/v1/trainings/me/?limit=100"
        case .getTrainingWithID(let id):
            return "/api/v1/trainings/\(id)"
        case .addMyTraining:
            return "/api/v1/trainings/me"
        case .deleteMytraining(let id):
            return "/api/v1/trainings/me/\(id)"
        case .getMyRoutes:
            return "/api/v1/routes/me/?limit=100"
        case .getRoutes:
            return "/api/v1/routes/?limit=100"
        case .getRouteWithID(let id):
            return "/api/v1/routes/\(id)"
        case .postMyRoute:
            return "/api/v1/routes/me"
        case .postRouteTime(let id, _,_):
            return "/api/v1/routes/\(id)/record"
        case .getAllRecordsForRoute(let id):
            return "/api/v1/routes/\(id)/records/?limit=100"
        case .deleteRoute(let id):
            return "/api/v1/routes/\(id)"
        case .putGroupTraining(let training_id, _, _, _, _, _, _, _, _, _, _):
            return "/api/v1/grouptrainings/\(training_id)"
        case .groupTrainingUsersList(let id, let type):
            return "/api/v1/grouptrainings/\(id)/\(type)"
        case .forgotPass:
            return "/api/v1/auth/password/email/"
        case .getSettingsMe:
            return "/api/v1/settings/me"
        case .putSettingsMe:
            return "/api/v1/settings/me"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .loginUser, .registerUser, .postUserMe, .changePassword, .addFriend, .groupTrainingDecision, .addGroupTraining, .addMyTraining, .postMyRoute, .postRouteTime, .postUserAvatar, .forgotPass:
            return .post
        case .deleteMytraining, .deleteGroupTraining, .deleteRoute, .removeInbox:
            return .delete
        case .putGroupTraining, .putSettingsMe:
            return .put
        default:
            return .get
        }
    }

    var parameters: [String: Any]? {
        switch self {
        case .loginUser(let email, let password):
            return [
                "email": email as AnyObject,
                "password": password as AnyObject
            ]
        case .registerUser(let first_name, let last_name, let email, let password, let birth, let gender):
            return [
                "first_name": first_name as AnyObject,
                "last_name": last_name as AnyObject,
                "email": email as AnyObject,
                "password": password as AnyObject,
                "birthday": birth as AnyObject,
                "gender": gender as AnyObject
            ]
        case .confirmEmail(let email):
            return [
                "email": email
            ]
        case .postUserMe(let first_name, let last_name, let email, let birth, let gender, let weight, let height, let unit, let onesignal_id):
            var params: [String : AnyObject] = [:]
            params["first_name"] = first_name as AnyObject?
            params["last_name"] = last_name as AnyObject?
            params["email"] = email as AnyObject?
            params["birthday"] = birth as AnyObject?
            params["gender"] = gender as AnyObject?
            params["weight"] = weight as AnyObject?
            params["height"] = height as AnyObject?
            params["unit"] = unit as AnyObject?
            params["onesignal_id"] = onesignal_id as AnyObject?
            return params
        case .changePassword(let oldPassword, let newPassword):
            return [
                "password_old": oldPassword as AnyObject,
                "password_new": newPassword as AnyObject
            ]
        case .groupTrainingDecision(_, let decision):
            return [
                "decision": decision as AnyObject
            ]
        case .addMyTraining(let route, let calories, let sport_id, let distance, let duration, let average_speed, let average_pace, let max_speed, let max_pace, let notes):
            return [
                "route": Mapper().toJSONArray(route),
                "calories": calories,
                "sport_id": sport_id,
                "distance": distance.isNaN == false ? distance : 0,
                "duration": duration,
                "average_speed": average_speed.isNaN == false ? average_speed : 0,
                "average_pace": average_pace.isNaN == false ? average_pace : 0,
                "max_speed": max_speed.isNaN == false ? max_speed : 0,
                "max_pace": max_pace.isNaN == false ? max_pace : 0,
                "notes": notes
            ]
        case .addGroupTraining(let sport_id, let title, let location, let private_state, let start_date, let start_time, let description, let repeat_type, let invites_user, let repeat_days):
            var params: [String : AnyObject] = [:]
            params["sport_id"] = sport_id as AnyObject?
            params["title"] = title as AnyObject?
            params["location"] = location?.toJSON() as AnyObject?
            params["private_state"] = private_state as AnyObject?
            params["start_date"] = start_date as AnyObject?
            params["start_time"] = start_time as AnyObject?
            params["description"] = description as AnyObject?
            params["repeat_type"] = repeat_type as AnyObject?
            params["invites_user"] = invites_user as AnyObject?
            params["repeat_days"] = repeat_days as AnyObject?
            return params
        case .putGroupTraining(_, let sport_id, let title, let location, let private_state, let start_date, let start_time, let description, let repeat_type, let invites_user, let repeat_days):
            var params: [String : AnyObject] = [:]
            params["sport_id"] = sport_id as AnyObject?
            params["title"] = title as AnyObject?
            params["location"] = location?.toJSON() as AnyObject?
            params["private_state"] = private_state as AnyObject?
            params["start_date"] = start_date as AnyObject?
            params["start_time"] = start_time as AnyObject?
            params["description"] = description as AnyObject?
            params["repeat_type"] = repeat_type as AnyObject?
            params["invites_user"] = invites_user as AnyObject?
            params["repeat_days"] = repeat_days as AnyObject?
            return params
        case .postMyRoute(let title, let sport_id, let private_state, let distance, let center, let route):
            return [
                "route": Mapper().toJSONArray(route),
                "title": title,
                "sport_id": sport_id,
                "private_state": private_state,
                "distance": distance.isNaN == false ? distance : 0,
                "center": [
                    "lat": "\(center.lat)",
                    "lon": "\(center.lon)"
                ]
            ]
        case .addFriend(_, let decision):
            return [
                "decision": decision as AnyObject
            ]
        case .postRouteTime(let sportID, let duration, _):
            return [
                "sport_id": sportID as AnyObject,
                "duration_seconds": duration as AnyObject
            ]
        case .forgotPass(let email):
            return [
                "email": email as AnyObject
            ]
        case .putSettingsMe(let invitation_notifications,let friend_request_notifications):
            return [
                "invitation_notifications": invitation_notifications as AnyObject,
                "friend_request_notifications": friend_request_notifications as AnyObject
            ]

        default:
            return nil
        }
    }
    
    var sampleData: Data {
        return Data()
    }

    var headers: [String: String]? {
        guard let token = keychain.get("token_sp") else {
            return nil
        }
        return ["Authorization": "Bearer \(token)"]
    }
    
    var url: String {
        return "\(baseURL)\(path)"
    }
    
    var multipartBody: [Moya.MultipartFormData]? {
        switch self {
        case .postUserAvatar(let avatar):
            let imageData = UIImageJPEGRepresentation(avatar, 5)
            return [MultipartFormData(provider: .data(imageData!), name: "picture", fileName: "picture.jpg", mimeType:"image/jpeg")]
        default:
            return nil
        }
    }

}
