//
//  TrainingMapController.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 17/10/2016.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

extension TrainingVC: MKMapViewDelegate {
    func drawRoute() {
        if routeID != nil {
            self.map.addPolygon(points: self.route)
            polyline = MKPolyline(coordinates: &self.route, count: self.route.count)
        }
    }
    
    func createPolyline(_ mapView: MKMapView, points: [CLLocationCoordinate2D]) {
        var points = points
        let centerPoint = points.last
        polylineOverlay = MKPolyline(coordinates: &points, count: points.count)
        map!.add(polylineOverlay)
        UIView.animate(withDuration: 1.5, animations: { () -> Void in
            let span = MKCoordinateSpanMake(0.0005, 0.0005)
            let region = MKCoordinateRegion(center: centerPoint!, span: span)
            self.map!.setRegion(region, animated: true)
        })
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return self.map.lineView(rendererForOverlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if locationShownOnce == false {
            var mapRegion = MKCoordinateRegion()
            mapRegion.center = map!.userLocation.coordinate
            mapRegion.span.latitudeDelta = 0.09
            mapRegion.span.longitudeDelta = 0.09
            map!.setRegion(mapRegion, animated: true)
            locationShownOnce = true
        }
    }
}
