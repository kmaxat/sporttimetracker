//
//  Settings.swift
//  SportTimeTracker
//
//  Created by kmaxat on 4/1/17.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import ObjectMapper


class Settings: Mappable {

    var invitation_notifications = Bool()
    var friend_request_notifications = Bool()
    
    
    init () {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        invitation_notifications <- map["invitation_notifications"]
        friend_request_notifications <- map["friend_request_notifications"]
    }
}
