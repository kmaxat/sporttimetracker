//
//  HystoryVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 10.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift
import PKHUD
import  Nuke

class HystoryTC: UITableViewController, ClearSelectionProtocol {
    
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var txtDistance: UILabel!
    @IBOutlet weak var txtTime: UILabel!
    @IBOutlet weak var txtCalories: UILabel!
    let bag = DisposeBag()
    var data = [DataHystory]()
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(self.getData), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!)
        
        topView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100)
        
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.rowHeight = UITableViewAutomaticDimension
        
        let calendarImage = UIImage(named: "calendar")
        let calendarBtn = UIButton(type: .custom)
        calendarBtn.bounds = CGRect(x: 0, y: 0, width: 30, height: 30)
        calendarBtn.setImage(calendarImage, for: UIControlState())
        let calendarButton   = UIBarButtonItem(customView: calendarBtn)
        navigationItem.rightBarButtonItems = [calendarButton]
        
        calendarBtn.rx.tap.bindNext {
            DatePickerDialog().show("Сортировать по месяцам", doneButtonTitle: "Готово", cancelButtonTitle: "Отмена", datePickerMode: .date) {
                (date) -> Void in
                let formatter = DateFormatter()
                formatter.dateFormat = "dd MMMM yyyy"
                self.txtTitle.text = formatter.string(from: date)
                let calendar = NSCalendar.current
                let hystory = Array(self.realm.objects(DataHystory.self)).filter({ training in
                    let components = calendar.dateComponents([.month, .year], from: dateFromString(training.date))
                    let componentsTo = calendar.dateComponents([.month, .year], from: dateFromString(training.date))
                    return components.month == componentsTo.month && components.year == componentsTo.year
                })
                
                self.data = hystory
                self.fillVIew(hystory)
                self.tableView.reloadData()
            }
        }.addDisposableTo(bag)
    }
    
    func getData() {
        self.txtTitle.text = "За все время"
        let hystory = Array(realm.objects(DataHystory.self))
        print(hystory.count)
        self.fillVIew(hystory.reversed())
        self.refreshControl?.endRefreshing()
        Network.request(target: .getMyTrainings())
            .mapObject(Hystorys.self)
            .subscribe(onNext: { (hystorys) in
                if hystorys.data != nil {
                    self.fillVIew(hystorys.data ?? [])
                    try! self.realm.write {
                        self.realm.add(hystorys.data ?? [], update: true)
                    }
                }
                }, onError: { (error) in
                    
                }, onCompleted: { 
                    
                }, onDisposed: nil).addDisposableTo(bag)
    }
    
    func fillVIew (_ hystory: [DataHystory]) {
        self.data = hystory
        let calculator = IndicatorsCalculator()

        let distance = stringKM(calculator._distance(inputDistance: hystory.reduce(0) {$0 + $1.distance}))
        let time = hystory.reduce(0) {$0 + $1.duration}
        let calories = hystory.reduce(0) {$0 + $1.calories}
        
        self.txtDistance.text = "\(distance)"
        self.txtTime.text = stringFromTimeInterval(sec: time)
        self.txtCalories.text = "\(calories)"
        
        self.tableView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearTC(self)
        self.setNavigationBarItem()
        
        self.getData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                                               for: indexPath) as! HystoryCell
        let training = self.data[(indexPath as NSIndexPath).row]
        let calculator = IndicatorsCalculator()

        cell.txtDate.text = to_show_stringFromDate(training.date)
        cell.txtCalories.text = "\(training.calories)"
        cell.txtDistance.text =  stringKM(calculator._distance(inputDistance: training.distance))
        cell.txtTime.text = stringFromTimeInterval(sec: training.duration)
        cell.imgSport.image = nil
        Nuke.loadImage(with: URL(string: training.sport!.picture)!, into: cell.imgSport)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let training = self.data[indexPath.row]
        let detailTrainingVC = st.instantiateViewController(withIdentifier: "RouteInfo") as! RouteDetailVC
        detailTrainingVC.routeID = training.id
        detailTrainingVC.hideBtn = true
        self.navigationController?.pushViewController(detailTrainingVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}
