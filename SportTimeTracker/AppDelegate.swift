//
//  AppDelegate.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 08.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import RealmSwift
import Simplicity
import Fabric
import Crashlytics
import SlideMenuControllerSwift
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UINavigationBar.appearance().barTintColor = Constants.colors.appMainColor
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        UINavigationBar.appearance().isTranslucent = false

        SlideMenuOptions.leftViewWidth = (self.window?.frame.width)! - 40;
        SlideMenuOptions.panFromBezel = false
        SlideMenuOptions.hideStatusBar = false
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        Fabric.with([Crashlytics.self])
        
        if keychain.get("token_sp") != nil {
            MainWindowController.setLeft()
            print(keychain.get("token_sp")!)
        } else {
            MainWindowController.setPages()
        }
        
//        OneSignal.initWithLaunchOptions(
//            launchOptions,
//            appId: "4a40146c-518f-11e5-bb32-fb90e87d5d6b",
//            settings: [kOSSettingsKeyAutoPrompt : false]
//        )
        
        
        OneSignal.initWithLaunchOptions(launchOptions, appId: "4a40146c-518f-11e5-bb32-fb90e87d5d6b", handleNotificationReceived: { (notification) in
        }, handleNotificationAction: { (result) in
            let payload: OSNotificationPayload? = result?.notification.payload
            
            var fullMessage: String? = payload?.body
            if payload?.additionalData != nil {
                var additionalData: [AnyHashable: Any]? = payload?.additionalData
                if additionalData!["actionSelected"] != nil {
                    fullMessage = fullMessage! + "\nPressed ButtonId:\(additionalData!["actionSelected"])"
                }
            }

        }, settings: [kOSSettingsKeyAutoPrompt : false])
        

        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if url.absoluteString.contains("fb") {
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        } else {
            return Simplicity.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
}

