//
//  SignalCollector.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 22/08/16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift

class SignalCollector: NSObject {
    
    var route = [CLLocationCoordinate2D]()
    var locationSignals = [CLLocation]()
    var calculator = IndicatorsCalculator()
    var calculators = [IndicatorsCalculator]()
    var startDate: Date?
    var time = 0
    let location = PublishSubject<CLLocation>()
    
    static let sharedInstance = SignalCollector()
    fileprivate override init() {
        super.init()
        ZJLocationService.time = 1
        bindLocations()
    }
    
    func bindLocations() {
        ZJLocationService.sharedManager.didUpdateLocation = { [weak self] newLocation in
            self?.locationSignals.append(newLocation)
            self?.fillData()
            self?.location.on(.next(newLocation))
        }
    }
    
    func fillData() {
        self.route = self.locationSignals.map{ $0.coordinate }
        self.calculator.seconds = time
        self.calculator.locations = self.locationSignals
        self.calculator.calculateIndicators()
        self.calculators.append(self.calculator)
    }
    
    func start() {
        ZJLocationService.startLocation()
    }
    
    func stop() {
        ZJLocationService.stopLocation()
    }
}
