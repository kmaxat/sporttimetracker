//
//  Notifications.swift
//  SportTimeTracker
//
//  Created by kmaxat on 4/1/17.
//  Copyright © 2017 vadim vitvickiy. All rights reserved.
//

import UIKit
import Pages
import SlideMenuControllerSwift
import RealmSwift
import RxSwift
import PKHUD
import ALCameraViewController
import Nuke

class NotificationsTC: UITableViewController, ClearSelectionProtocol, UITextFieldDelegate {
    
    @IBOutlet weak var groupInvitations: UISwitch!
    @IBOutlet weak var friendRequests: UISwitch!
    


    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearTC(self)
    }
    @IBAction func groupTrainingsButton(_ sender: Any) {
        HUD.show(.progress)
        Network.request(target: .putSettingsMe(invitation_notifications: groupInvitations.isOn, friend_request_notifications: friendRequests.isOn))
            .mapObject(Settings.self)
            .subscribe(onNext: { (response) in
                print(response)
                
            }, onError: { (error) in
                HUD.hide()
            }, onCompleted: {
                HUD.hide()
            }, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    @IBAction func friendRequestButton(_ sender: Any) {
        HUD.show(.progress)
        Network.request(target: .putSettingsMe(invitation_notifications: groupInvitations.isOn, friend_request_notifications: friendRequests.isOn))
            .mapObject(Settings.self)
            .subscribe(onNext: { (response) in
                print(response)
                
            }, onError: { (error) in
                HUD.hide()
            }, onCompleted: {
                HUD.hide()
            }, onDisposed: nil).addDisposableTo(rx_disposeBag)

    }
    
    
    
    func getData () {
        HUD.show(.progress)
        Network.request(target: .getSettingsMe())
            .mapObject(Settings.self)
            .subscribe(onNext: { (settings) in
                self.groupInvitations.setOn(settings.invitation_notifications, animated: true)
                self.friendRequests.setOn(settings.friend_request_notifications, animated: true)
                
            }, onError: { (error) in
                HUD.hide()
            }, onCompleted: {
                HUD.hide()
            }, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    
    func postUser() {
        HUD.show(.progress)
//        Network.request(target: .postUserMe(first_name: self.user.first_name, last_name: self.user.last_name, email: self.user.email, birth: self.user.birthday, gender: self.user.gender, weight: "\(self.user.weight)", height: "\(self.user.height)", unit: "\(self.user.unit)", onesignal_id: ""))
//            .subscribe(onNext: { (response) in
//                if response.statusCode < 300 {
//                    showAlert("SportTime", message: "Сохранено", controllder: self)
//                    keychain.set(self.user.unit.rawValue, forKey: "unit")
//                    
//                    
//                    
//                } else {
//                    showAlert("Error", message: "Error", controllder: self)
//                }
//            }, onError: { (error) in
//                HUD.hide()
//            }, onCompleted: {
//                HUD.hide()
//            }, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    
}
