//
//  RegistrationVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 09.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import DateTools
import RxSwift
import PKHUD

class RegistrationVC: UIViewController, UITextFieldDelegate {

    @IBOutlet fileprivate weak var closeBtn: UIButton!
    @IBOutlet fileprivate weak var txtName: UITextField!
    @IBOutlet fileprivate weak var txtEmail: UITextField!
    @IBOutlet fileprivate weak var txtPassword: UITextField!
    @IBOutlet fileprivate weak var txtDate: UITextField!
    @IBOutlet fileprivate weak var genderSwitch: UISegmentedControl!
    @IBOutlet fileprivate weak var registerBtn: UIButton!
    var datePickerView: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-M-dd"

        closeBtn.rx.tap.bindNext({
            self.dismiss(animated: true, completion: nil)
        }).addDisposableTo(rx_disposeBag)
        
        registerBtn.rx.tap.bindNext({
            if self.validateCredentials(name: self.txtName.text, email: self.txtEmail.text, password: self.txtPassword.text, birth: self.txtDate.text) {
                let strDate = dateFormatter.string(from: self.datePickerView.date)
                let wordsArr = self.txtName.text?.components(separatedBy: " ")
                let firstName = wordsArr?.first ?? ""
                let lastName = wordsArr!.count > 1 ? wordsArr?[1] ?? "" : ""
                let email = self.txtEmail!.text!
                let password = self.txtPassword!.text!
                let birth = strDate
                let gender = self.genderSwitch!.selectedSegmentIndex == 0 ? "male" : "female"
                HUD.show(.progress)
                Network.request(target: .registerUser(first_name: firstName, last_name: lastName, email: email, password: password, birth: birth, gender: gender))
                    .mapObject(Token.self)
                    .subscribe(onNext: { (token) in
                        if token.token != nil {
                            print(token.token!)
                            keychain.set(token.token!, forKey: "token_sp", withAccess: .accessibleAfterFirstUnlock)
                            keychain.set((self.txtName?.text)!, forKey: "first_name", withAccess: .accessibleAfterFirstUnlock)
                            keychain.set("", forKey: "last_name", withAccess: .accessibleAfterFirstUnlock)
                            keychain.set(self.txtEmail!.text!, forKey: "email", withAccess: .accessibleAfterFirstUnlock)
                            keychain.set(self.txtPassword!.text!, forKey: "password", withAccess: .accessibleAfterFirstUnlock)
                            keychain.set("\(self.genderSwitch!.selectedSegmentIndex)", forKey: "gender", withAccess: .accessibleAfterFirstUnlock)
                            Network.request(target: .confirmEmail(email: self.txtEmail!.text!)).subscribe().addDisposableTo(self.rx_disposeBag)
                            Network.request(target: .getUserMe())
                                .mapObject(User.self)
                                .subscribe(onNext: { (user) in
                                    keychain.set("\(user.id)", forKey: "id", withAccess: .accessibleAfterFirstUnlock)
                                    keychain.set(user.unit.rawValue, forKey: "unit", withAccess: .accessibleAfterFirstUnlock)
                                    self.view.endEditing(true)
                                    MainWindowController.setLeft()
                                    }, onError: nil, onCompleted: nil, onDisposed: nil).addDisposableTo(self.rx_disposeBag)
                        } else {
                            showErrorAlertWithCode(code: "Введены неверные данные, или эмейл уже занят", controller: self)
                        }
                        }, onError: { (error) in
                            HUD.hide()
                            showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                        }, onCompleted: { 
                            HUD.hide()
                        }, onDisposed: nil).addDisposableTo(self.rx_disposeBag)
            } else {
                HUD.hide()
                showAlert("SportTime", message: "Не все обязательные поля заполнены", controllder: self)
            }
        }).addDisposableTo(rx_disposeBag)
    }
    
    func validateCredentials(name: String?, email: String?, password: String?, birth: String?
        ) -> Bool {
        
        let emailRule = (email?.characters.count)! > 5
        let passwordRule = (password?.characters.count)! >= 6
        let birthRule = (birth?.characters.count)! > 0
        
        return emailRule && passwordRule && birthRule
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.txtName!:
            self.txtEmail?.becomeFirstResponder()
            break
        case self.txtEmail!:
            self.txtPassword?.becomeFirstResponder()
            break
        case self.txtPassword!:
            self.txtDate?.becomeFirstResponder()
            break
        default:
            break
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtDate {
            datePickerView = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.date
            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
        }
        return true
    }
    
    func datePickerValueChanged(_ sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        txtDate!.text = dateFormatter.string(from: sender.date)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.view.frame.origin.y < 0 {
            animateViewMoving(false, moveValue: 100)
        }
    }
    
    func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
}
