//
//  InboxVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 27.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import PKHUD
import Nuke

class InboxVC: UITableViewController, ClearSelectionProtocol {
    
    var data = [inboxData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNavigationBarItem()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(self.getData), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!)
        
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func getData() {
        Network.request(target: .inbox())
            .mapObject(Inbox.self)
            .subscribe(onNext: { (inbox) in
                self.data = inbox.data ?? []
                self.tableView.reloadData()
                
                }, onError: { (error) in
                    self.refreshControl?.endRefreshing()
                }, onCompleted: { 
                    self.refreshControl?.endRefreshing()
                }, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearTC(self)
        getData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let inbox = data[(indexPath as NSIndexPath).row]
        var userName = ""
        let date = to_show_stringFromDate(inbox.date) 
        
        if inbox.type == .friend_request {
            let cell = tableView.dequeueReusableCell(withIdentifier: "friend_request",
                                                               for: indexPath) as! InboxFriendshipCell
            cell.user = inbox.user
            userName = inbox.user!.first_name + " " + inbox.user!.last_name
            cell.img.image = nil
            if let url = URL(string: inbox.user?.picture ?? "") {
                Nuke.loadImage(with: url, into: cell.img)
            } else {
                cell.img.image = #imageLiteral(resourceName: "avatar_placeholder")
            }
            cell.txtText.text = userName
            cell.date.text = date
            
            cell.btnOptions.rx.tap.bindNext({
                self.showOptionsForFriendship(cell, indexPath: indexPath)
            }).addDisposableTo(cell.disposeBagCell)
            
            return cell
        } else if inbox.type == .group_training_invitation {
            let cell = tableView.dequeueReusableCell(withIdentifier: "group_training_invitation",
                                                               for: indexPath) as! InboxTrainingInviteCell
            
            cell.training = inbox.group_training
            
            userName = inbox.group_training!.user.first_name + " " + inbox.group_training!.user.last_name
            cell.date.text = date
            
            let trainingTitle = (inbox.group_training!.title)
            cell.img.image = nil
            if let url = URL(string: inbox.group_training?.user.picture ?? "") {
                Nuke.loadImage(with: url, into: cell.img)
            } else {
                cell.img.image = #imageLiteral(resourceName: "avatar_placeholder")
            }
            cell.txtText.text = userName + " пригласил вас на тренировку " + trainingTitle
            
            cell.btnOptions.rx.tap.bindNext({
                self.showOptionsForTraining(cell, indexPath: indexPath)
            }).addDisposableTo(cell.disposeBagCell)
            
            return cell
        } else if inbox.type == .group_training_update {
            let cell = tableView.dequeueReusableCell(withIdentifier: "group_training_update",
                                                               for: indexPath) as! InboxTrainingUdateCell
            
            cell.training = inbox.group_training
            cell.date.text = date
            userName = inbox.group_training!.user.first_name + " " + inbox.group_training!.user.last_name
            
            let trainingTitle = (inbox.group_training!.title)
            
            if inbox.updated_field == .changed_datetime {
                cell.txtText.text = userName + " изменил время в тренировке " + trainingTitle
            } else if inbox.updated_field == .changed_else {
                cell.txtText.text = userName + " изменил данные в тренировке " + trainingTitle
            } else if inbox.updated_field == .changed_location {
                cell.txtText.text = userName + " изменил место в тренировке " + trainingTitle
            } else if inbox.updated_field == .changed_sport {
                cell.txtText.text = userName + " изменил спорт в тренировке " + trainingTitle
            }
            
            cell.img.image = nil
            if let url = URL(string: inbox.group_training?.user.picture ?? "") {
                Nuke.loadImage(with: url, into: cell.img)
            } else {
                cell.img.image = #imageLiteral(resourceName: "avatar_placeholder")
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let inbox = data[(indexPath as NSIndexPath).row]
        if inbox.type == .friend_request {
            let profileVC = st.instantiateViewController(withIdentifier: "User") as! UserProfileVC
            profileVC.userID = inbox.user!.id
            profileVC.hideCloseBtn = true
            self.navigationController?.pushViewController(profileVC, animated: true)
        } else {
            let infoVC = st.instantiateViewController(withIdentifier: "TrainingInfo") as! TrainingDetailVC
            infoVC.trainingID = inbox.group_training!.id
            self.navigationController?.pushViewController(infoVC, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func showOptionsForTraining (_ cell: InboxTrainingInviteCell, indexPath: IndexPath) {
        let actionSheetController: UIAlertController = UIAlertController(title: "Выберите действие", message: "", preferredStyle: .actionSheet)
        let goAction: UIAlertAction = UIAlertAction(title: "Иду", style: .default) { action -> Void in
            self.trainingInvite(cell.training.id, decision: "accepted", cell: cell, indexPath: indexPath)
        }
        actionSheetController.addAction(goAction)
        let mbAction: UIAlertAction = UIAlertAction(title: "Возможно", style: .default) { action -> Void in
            self.trainingInvite(cell.training.id, decision: "maybe", cell: cell, indexPath: indexPath)
        }
        actionSheetController.addAction(mbAction)
        let declineAction: UIAlertAction = UIAlertAction(title: "Отказаться", style: .default) { action -> Void in
            self.trainingInvite(cell.training.id, decision: "rejected", cell: cell, indexPath: indexPath)
        }
        actionSheetController.addAction(declineAction)
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Отмена", style: .cancel) { action -> Void in}
        actionSheetController.addAction(cancelActionButton)
        present(actionSheetController, animated: true, completion: nil)
    }
    
    func showOptionsForFriendship (_ cell: InboxFriendshipCell, indexPath: IndexPath) {
        let actionSheetController: UIAlertController = UIAlertController(title: "Выберите действие", message: "", preferredStyle: .actionSheet)
        let invite: UIAlertAction = UIAlertAction(title: "Принять", style: .default) { action -> Void in
            HUD.show(.progress)
            Network.request(target: .addFriend(id: cell.user.id, decision: "accepted"))
                .subscribe(onNext: { (response) in
                    if response.statusCode == 200 {
                        self.removeInbox(id: self.data[indexPath.row].id, indexPath: indexPath)
                        showAlert("SportTime", message: "Запрос принят", controllder: self)
                    } else {
                        showErrorAlertWithCode(code: response.statusCode, controller: self)
                    }
                    }, onError: { (error) in
                        HUD.hide()
                        showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                    }, onCompleted: { 
                        HUD.hide()
                    }, onDisposed: nil).addDisposableTo(cell.disposeBagCell)
        }
        actionSheetController.addAction(invite)
        
        let decline: UIAlertAction = UIAlertAction(title: "Отклонить", style: .default) { action -> Void in
            HUD.show(.progress)
            Network.request(target: .addFriend(id: cell.user.id, decision: "remove"))
                .subscribe(onNext: { (response) in
                    if response.statusCode == 200 {
                        self.removeInbox(id: self.data[indexPath.row].id, indexPath: indexPath)
                        showAlert("SportTime", message: "Запрос отклонен", controllder: self)
                    } else {
                        showErrorAlertWithCode(code: response.statusCode, controller: self)
                    }
                    }, onError: { (error) in
                        HUD.hide()
                        showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                    }, onCompleted: { 
                        HUD.hide()
                    }, onDisposed: nil).addDisposableTo(cell.disposeBagCell)
        }
        actionSheetController.addAction(decline)
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Отмена", style: .cancel) { action -> Void in}
        actionSheetController.addAction(cancelActionButton)
        present(actionSheetController, animated: true, completion: nil)
    }

    func trainingInvite(_ trainingID: Int, decision: String, cell: InboxTrainingInviteCell, indexPath: IndexPath) {
        HUD.show(.progress)
        Network.request(target: .groupTrainingDecision(id: trainingID, decision: decision))
            .subscribe(onNext: { (response) in
                if response.statusCode == 200 {
                    self.removeInbox(id: self.data[indexPath.row].id, indexPath: indexPath)
                    showAlert("SportTime", message: "Запрос принят", controllder: self)
                } else {
                    showErrorAlertWithCode(code: response.statusCode, controller: self)
                }
                }, onError: { (error) in
                    HUD.hide()
                    showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                }, onCompleted: { 
                    HUD.hide()
                }, onDisposed: nil).addDisposableTo(cell.disposeBagCell)
    }
    
    func removeInbox(id: Int, indexPath: IndexPath) {
        Network.request(target: .removeInbox(id: id))
        .bindNext { (response) in
            if response.statusCode < 300 {
                self.data.remove(at: (indexPath.row))
                self.tableView.deleteRows(at: [indexPath], with: .left)
                self.tableView.reloadData()
            }
        }.addDisposableTo(rx_disposeBag)
    }
}


