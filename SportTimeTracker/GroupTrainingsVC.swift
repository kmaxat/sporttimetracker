//
//  GroupTrainingsVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 10.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import PageMenu
import RxSwift

class GroupTrainingsVC: UIViewController {
    
    var pageMenu : CAPSPageMenu?
    
    let bag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Групповые тренировки"
        
        var controllerArray : [UIViewController] = []
        
        let groupList = st.instantiateViewController(withIdentifier: "groupList") as! GroupListTC
        groupList.title = "Мои тренировки"
        groupList.rootVC = self
        controllerArray.append(groupList)
        
        let groupMap = st.instantiateViewController(withIdentifier: "groupMap") as! GroupMap
        groupMap.rootVC = self
        groupMap.title = "Поиск"
        controllerArray.append(groupMap)
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(Constants.colors.appMainColor),
            .menuItemSeparatorPercentageHeight(0.0),
            .useMenuLikeSegmentedControl(true)
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
        
        let calendarImage = UIImage(named: "calendar")
        let calendarBtn = UIButton(type: .custom)
        calendarBtn.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
        calendarBtn.setImage(calendarImage, for: UIControlState())
        let calendarButton   = UIBarButtonItem(customView: calendarBtn)
        
        let addBtn = UIButton(type: .contactAdd)
        let addButton   = UIBarButtonItem(customView: addBtn)
        
        addBtn.rx.tap.bindNext {
            let trainingCreate = st.instantiateViewController(withIdentifier: "trainingCreate")
            self.navigationController?.pushViewController(trainingCreate, animated: true)
        }.addDisposableTo(bag)
        
        calendarBtn.rx.tap.bindNext { 
            DatePickerDialog().show("Тренировки на дату", doneButtonTitle: "Готово", cancelButtonTitle: "Отмена", datePickerMode: .date) {
                (date) -> Void in
                groupMap.getDataWithDate(to_send_stringFormatDateToServer(date))
            }
        }.addDisposableTo(bag)
        
        navigationItem.rightBarButtonItems = [addButton, calendarButton]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

}
