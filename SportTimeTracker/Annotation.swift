//
//  Annotation.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 15.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import MapKit

class Annotation: NSObject, MKAnnotation {

    let title: String?
    let locationName: String?
    let coordinate: CLLocationCoordinate2D
    let training: DataGT!
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D, training: DataGT) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        self.training = training
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}
