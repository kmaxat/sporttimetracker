//
//  SportCell.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 16.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

class SportCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var txt: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
