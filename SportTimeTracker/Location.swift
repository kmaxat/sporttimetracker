//
//  Location.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper
import Realm
import RealmSwift

class LocationObject: Object, Mappable {
    
    dynamic var lat: Double = 0.0
    
    dynamic var lon: Double = 0.0

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        lat <- map["lat"]
        lon <- map["lon"]
    }
    

}
