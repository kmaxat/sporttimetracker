//
//  MyCompTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 10.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import PKHUD
import Nuke

class MyCompTC: UITableViewController {

    var data = [dataRoute]()
    let bag = DisposeBag()
    
    var rootVC: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(self.getData), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!)
        
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func getData () {
        HUD.show(.progress)
        Network.request(target: .getMyRoutes())
            .mapObject(Routes.self)
            .subscribe(onNext: { (routes) in
                HUD.hide()
                self.data = routes.data ?? []
                self.tableView.reloadData()
                self.refreshControl?.endRefreshing()
                }, onError: { (error) in
                    showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                }, onCompleted: { 
                    
                }, onDisposed: { 
                    
            }).addDisposableTo(bag)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                             for: indexPath) as! RouteCell
        let route = data[indexPath.row]
        let calculator = IndicatorsCalculator()

        cell.txtSportTitle.text = route.title
        cell.txtDistance.text = stringKM(calculator._distance(inputDistance: route.distance))
        cell.imgRoute.image = nil
        if let url = URL(string: route.picture ?? "") {
            Nuke.loadImage(with: url, into: cell.imgRoute)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let compDetail = st.instantiateViewController(withIdentifier: "compDetail") as! CompetitionDetailVC
        compDetail.routeID = self.data[(indexPath as NSIndexPath).row].id
        self.rootVC?.navigationController?.pushViewController(compDetail, animated: true)
//        let training = self.data[indexPath.row]
//        let detailTrainingVC = st.instantiateViewController(withIdentifier: "RouteInfo") as! RouteDetailVC
//        detailTrainingVC.routeID = training.id
//        detailTrainingVC.isFollowRoute = true
//        detailTrainingVC.training = training
//        detailTrainingVC.hideBtn = true
//        self.rootVC?.navigationController?.pushViewController(detailTrainingVC, animated: true)
    }
}
