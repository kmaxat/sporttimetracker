//
//  ShowIndicatorCell.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 22.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

class ShowIndicatorCell: UITableViewCell {

    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtIndicator: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
