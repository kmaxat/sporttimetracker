
//
//  ProfileTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 11.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import Pages
import SlideMenuControllerSwift
import RealmSwift
import RxSwift
import PKHUD
import ALCameraViewController
import Nuke

class ProfileTC: UITableViewController, ClearSelectionProtocol, UITextFieldDelegate {

    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet fileprivate weak var txtName: UILabel!
    @IBOutlet weak var txtEmail: UILabel!
    @IBOutlet fileprivate weak var txtBirth: UITextField!
    @IBOutlet fileprivate weak var txtWeight: UITextField!
    @IBOutlet fileprivate weak var txtGrowth: UITextField!
    @IBOutlet fileprivate weak var saveBtn: UIButton!
    @IBOutlet weak var avatarBtn: UIButton!
    @IBOutlet weak var segUnit: UISegmentedControl!

    var user = User()
    var datePickerView: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        binds()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearTC(self)
    }
    
    func binds() {
        txtWeight.rx.textInput.text.bindNext({ (str) in
            self.user.weight = str?.isEmpty == false ? Double(str!)! : 0
        }).addDisposableTo(rx_disposeBag)
        
        txtGrowth.rx.textInput.text.bindNext({ (str) in
            self.user.height = str?.isEmpty == false ? Int(str!)! : 0
        }).addDisposableTo(rx_disposeBag)
        
        
        segUnit.rx.value.bindNext({ (option) in
            switch option {
            case 0 :
                self.user.unit = unitUser(rawValue: "km")!
            case 1 :
                self.user.unit = unitUser(rawValue: "mi")!
            default:
                ()
            }
        }).addDisposableTo(rx_disposeBag)

        saveBtn.rx.tap.bindNext({
            self.postUser()
        }).addDisposableTo(rx_disposeBag)
        
        avatarBtn.rx.tap.bindNext {
            let cameraViewController = CameraViewController(croppingEnabled: true) { [weak self] image, asset in
                self?.dismiss(animated: true, completion: nil)
                if image != nil {
                    self?.postAvatar(RBResizeImage(image, targetSize: CGSize(width: 250, height: 250))!)
                }
            }
            self.present(cameraViewController, animated: true, completion: nil)
        }.addDisposableTo(rx_disposeBag)
    }
    
    func getData () {
        HUD.show(.progress)
        Network.request(target: .getUserMe())
            .mapObject(User.self)
            .subscribe(onNext: { (user) in
                self.user = user
                self.imgAvatar.image = nil
                if let url = URL(string: user.picture ?? "") {
                    Nuke.loadImage(with: url, into: self.imgAvatar)
                }else {
                    self.imgAvatar.image = #imageLiteral(resourceName: "avatar_placeholder")
                }
                self.txtName.text = user.first_name + " " + user.last_name
                self.txtEmail.text = user.email
                self.txtBirth.text = to_show_stringFromDate_no_time(user.birthday)
                self.txtWeight.text = "\(user.weight)"
                self.txtGrowth.text = "\(user.height)"
                
                switch user.unit {
                    case .km:
                        self.segUnit.selectedSegmentIndex = 0
                    case .mi:
                        self.segUnit.selectedSegmentIndex = 1
                }
                
                if user.verified == false {
                    let alertController = UIAlertController(title: "SportTime", message:
                        "Email не подтвержден выслать еще раз?", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Выслать еще раз", style: UIAlertActionStyle.default, handler: { (action) in
                        Network.request(target: .confirmEmail(email: self.txtEmail!.text!)).subscribe().addDisposableTo(self.rx_disposeBag)
                    }))
                    alertController.addAction(UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: { (action) in
                    }))
                    self.present(alertController, animated: true, completion: nil)
                }

                }, onError: { (error) in
                    HUD.hide()
                }, onCompleted: { 
                    HUD.hide()
                }, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    
    func postUser() {
        HUD.show(.progress)
        Network.request(target: .postUserMe(first_name: self.user.first_name, last_name: self.user.last_name, email: self.user.email, birth: self.user.birthday, gender: self.user.gender, weight: "\(self.user.weight)", height: "\(self.user.height)", unit: "\(self.user.unit)", onesignal_id: ""))
            .subscribe(onNext: { (response) in
                if response.statusCode < 300 {
                    showAlert("SportTime", message: "Сохранено", controllder: self)
                    keychain.set(self.user.unit.rawValue, forKey: "unit", withAccess: .accessibleAfterFirstUnlock)

                    

                    
                } else {
                    showAlert("Error", message: "Error", controllder: self)
                }
                }, onError: { (error) in
                    HUD.hide()
                }, onCompleted: { 
                    HUD.hide()
                }, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    
    func postAvatar(_ avatar: UIImage) {
        HUD.show(.progress)
        Network.request(target: .postUserAvatar(avatar: avatar))
            .subscribe(onNext: { (response) in
                if response.statusCode < 300 {
                    self.imgAvatar.image = RBResizeImage(avatar, targetSize: CGSize(width: 250, height: 250))!
                } else {
                    showErrorAlertWithCode(code: response.statusCode, controller: self)
                }
                }, onError: { (error) in
                    HUD.hide()
                    showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                }, onCompleted: { 
                    HUD.hide()
                }, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtBirth {
            datePickerView = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.date
            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
        }
    }
    
    func datePickerValueChanged(_ sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        txtBirth.text = dateFormatter.string(from: sender.date)
        self.user.birthday = to_send_stringFormatDateToServer(sender.date)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).row == 5 {
            showAlertChangePass()
        }
    }
    
    func showAlertChangePass() {
        var tFieldOldPass: UITextField!
        var tFieldNewPass: UITextField!
        
        func configurationTextFieldOldPass(_ textField: UITextField!)
        {
            textField.placeholder = "Старый пароль"
            tFieldOldPass = textField
        }
        
        func configurationTextFieldNewPass(_ textField: UITextField!)
        {
            textField.placeholder = "Новый пароль"
            tFieldNewPass = textField
        }
        
        func handleCancel(_ alertView: UIAlertAction!)
        {
            print("Cancelled !!")
        }
        
        let alert = UIAlertController(title: "Смена пароля", message: "", preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: configurationTextFieldOldPass)
        alert.addTextField(configurationHandler: configurationTextFieldNewPass)
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Готово", style: .default, handler:{ (UIAlertAction) in
            let pass = keychain.get("password")
            if (pass == nil || pass == tFieldOldPass.text) {
                self.changePass(tFieldOldPass.text!, new: tFieldNewPass.text!)
            } else {
                showAlert("SportTime", message: "Неверный старый пароль", controllder: self)
            }
        }))
        self.present(alert, animated: true, completion: {})
    }
    
    func changePass(_ old: String, new: String) {
        HUD.show(.progress)
        Network.request(target: .changePassword(oldPassword: old, newPassword: new))
        .subscribe(onNext: { (response) in
            if response.statusCode < 300 {
                showAlert("SportTime", message: "Пароль изменен", controllder: self)
            } else {
                showErrorAlertWithCode(code: response.statusCode, controller: self)
            }
            }, onError: { (error) in
                showErrorAlertWithCode(code: error.localizedDescription, controller: self)
            }, onCompleted: { 
                HUD.hide()
            }, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    
    @IBOutlet fileprivate weak var exitBtn: UIButton!
    
    @IBAction func exitBtnAction(_ sender: AnyObject) {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        keychain.delete("token_sp")
        keychain.delete("first_name")
        keychain.delete("last_name")
        keychain.delete("email")
        keychain.delete("password")
        keychain.delete("gender")
        keychain.delete("unit")
        keychain.delete("onesignal_id")
        
        var pages: PagesController?
        pages = pagesControllerInStoryboard()
        let mainViewController = pages!
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let leftViewController = st.instantiateViewController(withIdentifier: "left") as! LeftMenu
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
        appDelegate.window?.rootViewController = slideMenuController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func pagesControllerInStoryboard() -> PagesController {
        let storyboardIds = ["One","Two", "Three"]
        return PagesController(storyboardIds)
    }
}
