//
//  NearCompTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 10.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import PKHUD
import Nuke

class NearCompTC: UITableViewController, ClearSelectionProtocol {

    var data = [dataRoute]()
    var rootVC: UIViewController?
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(self.getData), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!)
        
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func getData() {
        HUD.show(.progress)
        Network.request(target: .getRoutes())
            .mapObject(Routes.self)
            .subscribe(onNext: { (response) in
                self.data = response.data ?? []
                self.tableView.reloadData()
                }, onError: { (error) in
                    HUD.hide()
                    self.refreshControl?.endRefreshing()
                    showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                }, onCompleted: {
                    HUD.hide()
                    self.refreshControl?.endRefreshing()
                }, onDisposed: nil).addDisposableTo(bag)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                                               for: indexPath) as! RouteCell
        let competition = data[indexPath.row]
        let calculator = IndicatorsCalculator()

        cell.txtSportTitle.text = competition.title
        cell.txtDistance.text = stringKM(calculator._distance(inputDistance: competition.distance))
        cell.imgRoute.image = nil
        if let url = URL(string: competition.picture ?? "") {
            Nuke.loadImage(with: url, into: cell.imgRoute)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let compDetail = st.instantiateViewController(withIdentifier: "compDetail") as! CompetitionDetailVC
        compDetail.routeID = self.data[(indexPath as NSIndexPath).row].id
        self.rootVC?.navigationController?.pushViewController(compDetail, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
