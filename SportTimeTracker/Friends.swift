
//
//  Friends.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper

class Friends: Mappable {
    
    var meta: Meta?

    var data: [DataFriends]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        meta <- map["meta"]
        data <- map["data"]
    }

}

class DataFriends: Mappable {

    var gender = ""

    var height: Int = 0

    var weight: Double = 0

    var id: Int = 0

    var friend = ""

    var last_name = ""

    var birthday = ""

    var email = ""

    var picture: String?

    var first_name = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        gender <- map["gender"]
        height <- map["height"]
        weight <- map["weight"]
        id <- map["id"]
        friend <- map["friend"]
        last_name <- map["last_name"]
        birthday <- map["total_pages"]
        email <- map["per_page"]
        picture <- map["count"]
        first_name <- map["first_name"]
    }

}

