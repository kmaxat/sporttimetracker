//
//  TrainingVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 10.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import Foundation
import RxSwift
import PKHUD
import Async
import AURUnlockSlider
import OneSignal

enum Indicator {
    case distance
    case time
    case speed
    case pace
    case avgSpeed
    case avgPace
}

class TrainingVC: UIViewController {
    
    @IBOutlet weak var imgSport: UIImageView!
    @IBOutlet weak var txtSport: UILabel!
    @IBOutlet weak var txtFirstIndicator: UILabel!
    @IBOutlet weak var txtSecondIndicator: UILabel!
    @IBOutlet weak var txtFirstIndication: UILabel!
    @IBOutlet weak var txtSecondIndication: UILabel!
    @IBOutlet weak var sportBtn: UIButton!
    @IBOutlet weak var firstIndBtn: UIButton!
    @IBOutlet weak var secondIndBtn: UIButton!
    @IBOutlet weak var timerBtn: UIButton!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var stopBtn: UIButton!
    @IBOutlet weak var pauseBtn: UIButton!
    @IBOutlet weak var map: MKMapView!
    var locationShownOnce = Bool()
    var routeOverlay: MKPolygon!
    var polylineOverlay: MKPolyline!
    var sport: dataSport?
    var firstIndicator: Indicator!
    var secondIndicator: Indicator!
    var timer: Observable<NSInteger>!
    var trainingStopped = Bool()
    let signalCollector = SignalCollector.sharedInstance
    var route = [CLLocationCoordinate2D]()
    var routeID: Int?
    var didShowAlert = Bool()
    var distanceCalculator = DistanceCalculator()
    var polyline: MKPolyline!
    var clearView: UIView!
    var unlockSlider: AURUnlockSlider!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstIndicator = .time
        secondIndicator = .distance
        binds()
        drawRoute()
        OneSignal.registerForPushNotifications()
        postUser()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if routeID == nil {
            self.setNavigationBarItem()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.didShowAlert = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.didShowAlert = true
    }
    
    func updateView(_ location: CLLocation) {
        let eventDate = location.timestamp
        let howRecent = eventDate.timeIntervalSinceNow

        if abs(howRecent) < 10.0 && location.horizontalAccuracy < 20 {
            self.createPolyline(self.map!, points: signalCollector.route)
            if self.routeID != nil {
                let didFollow = self.distanceCalculator.isCoordinate(location.coordinate, closeTo: self.polyline)
                if didFollow == false && didShowAlert == false {
                    showAlert("SportTime", message: "Вы сошли с маршрута", controllder: self)
                    showLocalPush("Вы сошли с маршрута")
                    didShowAlert = true
                    Async.main(after: 60, {
                        self.didShowAlert = false
                    })
                }
            }
            
            let distance = signalCollector.calculator._distance()
            let speed = signalCollector.calculator._speed()
            let pace = Int(signalCollector.calculator._pace())
            let avgSpeed = signalCollector.calculator._avgSpeed()
            let avgPace = Int(signalCollector.calculator._avgPace())
            
            switch self.firstIndicator! {
            case .distance:
                self.txtFirstIndication.text = stringKM(distance)
                break
            case .time:
                break
            case .speed:
                self.txtFirstIndication.text = stringSpeed(speed)
            case .pace:
                self.txtFirstIndication.text = stringPaceFromTimeInterval(sec: pace)
            case .avgSpeed:
                self.txtFirstIndication.text = stringSpeed(avgSpeed)
            case .avgPace:
                self.txtFirstIndication.text = stringPaceFromTimeInterval(sec: avgPace)
            }

            switch self.secondIndicator! {
            case .distance:
                self.txtSecondIndication?.text = stringKM(distance)
                break
            case .time:
                break
            case .speed:
                self.txtSecondIndication?.text = stringSpeed(speed)
            case .pace:
                self.txtSecondIndication?.text = stringPaceFromTimeInterval(sec: pace)
            case .avgSpeed:
                self.txtSecondIndication?.text = stringSpeed(avgSpeed)
            case .avgPace:
                self.txtSecondIndication?.text = stringPaceFromTimeInterval(sec: avgPace)
            }
        }
    }
    
    func clearVC() {
        signalCollector.calculator = IndicatorsCalculator()
        signalCollector.calculators = []
        signalCollector.locationSignals = []
        self.route = []
        signalCollector.time = 0
        signalCollector.startDate = nil
        self.map.removeOverlays(map!.overlays)
    }
    
    func createTraining() -> DataTR {
        let training = DataTR()
        training.average_speed = signalCollector.calculators.reduce(0) {$0 + $1._avgSpeed()} / Double(signalCollector.calculators.count)
        training.average_pace = signalCollector.calculators.reduce(0) {$0 + $1._avgPace()} / Double(signalCollector.calculators.count)
        training.date = to_send_stringFormatDateToServer_full(signalCollector.startDate!)
        training.route2d = signalCollector.route
        training.distance = signalCollector.calculators.last!.distance
        training.duration = signalCollector.calculators.last!.seconds
        training.max_pace = signalCollector.calculators.reduce(-Double.infinity, { max($0, $1._pace()) })
        training.max_speed = signalCollector.calculators.reduce(-Double.infinity, { max($0, $1.speed) })
        training.sport = self.sport!
        training.locations = signalCollector.calculators.last!.locations
        training.route = signalCollector.route.map {
            let location = LocationObject()
            location.lat = $0.latitude
            location.lon = $0.longitude
            return location
        }
        training.calories = signalCollector.calculators.last!.time * Double(sport!.calories)
        return training
    }
    
    func saveTraining(training: DataTR) {
        HUD.show(.progress)
        Network.request(target: .addMyTraining(route: training.route, calories: training.calories.roundToPlaces(places: 2), sport_id: training.sport.id, distance: training.distance.roundToPlaces(places: 2), duration: training.duration, average_speed: training.average_speed.roundToPlaces(places: 2), average_pace: training.average_pace.roundToPlaces(places: 2), max_speed: training.max_speed.roundToPlaces(places: 2), max_pace: training.max_pace.roundToPlaces(places: 2), notes: ""))
            .subscribe(onNext: { (response) in
                if response.statusCode != 200 {
                    showErrorAlertWithCode(code: response.statusCode, controller: self)
                } else {
                    
                    switch self.firstIndicator! {
                    case .distance:
                        self.txtFirstIndication.text = stringKM(0)
                        break
                    case .time:
                        self.txtFirstIndication?.text = "00:00"
                        break
                    case .speed:
                        self.txtFirstIndication.text = stringSpeed(0)
                    case .pace:
                        self.txtFirstIndication.text = stringPaceFromTimeInterval(sec: 0)
                    case .avgSpeed:
                        self.txtFirstIndication.text = stringSpeed(0)
                    case .avgPace:
                        self.txtFirstIndication.text = stringPaceFromTimeInterval(sec: 0)
                    }
                    
                    switch self.secondIndicator! {
                    case .distance:
                        self.txtSecondIndication?.text = stringKM(0)
                        break
                    case .time:
                        self.txtSecondIndication?.text = "00:00"
                        break
                    case .speed:
                        self.txtSecondIndication?.text = stringSpeed(0)
                    case .pace:
                        self.txtSecondIndication?.text = stringPaceFromTimeInterval(sec: 0)
                    case .avgSpeed:
                        self.txtSecondIndication?.text = stringSpeed(0)
                    case .avgPace:
                        self.txtSecondIndication?.text = stringPaceFromTimeInterval(sec: 0)
                    }
                    
                    
                    self.showTrainingDetail(training: training)
                }
            }, onError: { (error) in
                HUD.hide()
                showErrorAlertWithCode(code: error.localizedDescription, controller: self)
            }, onCompleted: {
                HUD.hide()
            }, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    
    func showTrainingDetail(training: DataTR) {
        let saveTrainingVC = st.instantiateViewController(withIdentifier: "RouteInfo") as! RouteDetailVC
        saveTrainingVC.training = training
        if self.routeID != nil {
            saveTrainingVC.routeID = self.routeID
            saveTrainingVC.isFollowRoute = true
        }
        self.navigationController?.pushViewController(saveTrainingVC, animated: true)
    }
    
    @IBAction func startTrainingBtn(_ sender: AnyObject) {
        if self.sport == nil {
            showAlert("SportTime", message: "Выберите спорт", controllder: self)
            return
        }
        let alertController = UIAlertController(title: "Тренировка сейчас начнется", message:
            "", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alertController, animated: true, completion: nil)
        let countdownArr = [3, 2, 1]
        for index in countdownArr {
            alertController.title = "\(index)"
            CFRunLoopRunInMode(CFRunLoopMode.defaultMode, 1, false)
        }
        alertController.dismiss(animated: true, completion: nil)
        configureLock()
        if signalCollector.startDate == nil {
            signalCollector.startDate = Date()
        }
        signalCollector.start()
    }
    
    @IBAction func pauseTraining(_ sender: AnyObject) {
        signalCollector.stop()
    }
    
    @IBAction func stopTrainingBtn(_ sender: AnyObject) {
        if signalCollector.locationSignals.count > 5 {
            let training = createTraining()
            saveTraining(training: training)
        }
        signalCollector.stop()
        self.trainingStopped = true
        clearVC()
    }
    
    func postUser() {
        let pushIsSet = keychain.get("onesignal_id")
        if(pushIsSet == nil){
            OneSignal.idsAvailable({(_ userId, _ pushToken) in
                let onesignalUserId = userId
                HUD.show(.progress)
                Network.request(target: .postUserMe(first_name: nil, last_name: nil, email: nil, birth: nil, gender: nil, weight: nil, height: nil, unit: nil, onesignal_id: onesignalUserId))
                    .subscribe(onNext: { (response) in
                        if response.statusCode != 200 {
                            showErrorAlertWithCode(code: response.statusCode, controller: self)
                        } else {
                            keychain.set(true, forKey: "onesignal_id")
                        }
                    }, onError: { (error) in
                        HUD.hide()
                    }, onCompleted: {
                        HUD.hide()
                    }, onDisposed: nil).addDisposableTo(self.rx_disposeBag)
            })
        }
        
    }

}
