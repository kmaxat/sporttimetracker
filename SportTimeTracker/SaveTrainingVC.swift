//
//  SaveTrainingVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 19.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import PKHUD

class SaveTrainingVC: UIViewController, UITextFieldDelegate, MKMapViewDelegate {
    
    @IBOutlet fileprivate weak var map: MKMapView!
    @IBOutlet fileprivate weak var name: UITextField!
    @IBOutlet fileprivate weak var privateStateSwitch: UISwitch!
    @IBOutlet fileprivate weak var txtDistance: UILabel!
    @IBOutlet fileprivate weak var addBtn: UIButton!
    
    var training: DataTR?
    let bag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        txtDistance.text = stringKM(training!.distance)
        map.addPolygon(points: training!.route2d)
        let maxLat = training?.route.reduce(-Double.infinity, { max($0, $1.lat) })
        let maxLong = training?.route.reduce(-Double.infinity, { max($0, $1.lon) })
        let minLat = training?.route.reduce(Double.infinity, { min($0, $1.lat) })
        let minLong = training?.route.reduce(Double.infinity, { min($0, $1.lon) })
        
        let center = CLLocationCoordinate2DMake((maxLat! + minLat!) * 0.5, (maxLong! + minLong!) * 0.5)
        
        let centerLocation = LocationObject()
        centerLocation.lat = center.latitude
        centerLocation.lon = center.longitude
        
        addBtn?.rx.tap.bindNext({
            HUD.show(.progress)
            let title = self.name!.text!
            let sportID = self.training!.sport.id
            let privateState = self.privateStateSwitch?.isOn == true ? "friends" : "close"
            let distance = self.training!.distance
            let route = self.training?.route
            Network.request(target: .postMyRoute(title: title, sport_id: sportID, private_state: privateState, distance: distance, center: centerLocation, route: route!))
                .subscribe(onNext: { (response) in
                    if response.statusCode == 200 {
                        _ = self.navigationController?.popToRootViewController(animated: true)
                    } else {
                        showErrorAlertWithCode(code: response.statusCode, controller: self)
                    }
                    }, onError: { (error) in
                        HUD.hide()
                    }, onCompleted: { 
                        HUD.hide()
                    }, onDisposed: nil).addDisposableTo(self.bag)
        }).addDisposableTo(bag)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return map.lineView(rendererForOverlay: overlay)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.view.frame.origin.y < 0 {
            animateViewMoving(false, moveValue: 100)
        }
    }
    
    func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
}
