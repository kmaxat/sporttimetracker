//
//  GroupTrainingCell.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 19.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

class GroupTrainingCell: UITableViewCell {

    @IBOutlet weak var sportImg: UIImageView!
    @IBOutlet weak var txtSport: UILabel!
    @IBOutlet weak var txtDate: UILabel!
    @IBOutlet weak var txtCount: UILabel!
    @IBOutlet weak var txtRepeat: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
