//
//  GroupTrainings.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper

class GroupTrainings: Mappable {
    
    var meta: Meta?

    var data: [DataGT]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        meta <- map["meta"]
        data <- map["data"]
    }
}

class DataGT: Mappable {

    var id: Int = 0
    
    var desc: String?

    var start_date = String()

    var repeat_days: [Int]?

    var repeat_type = String()

    var sport = dataSport()

    var user = User()

    var start_time = String()

    var title = String()

    var location = LocationObject()

    var private_state = String()

    var count_accepted: Int = 0

    var count_invited: Int = 0
    
    var count_maybe: Int = 0
    
    init () {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        desc <- map["description"]
        start_date <- map["start_date"]
        repeat_days <- map["repeat_days"]
        sport <- map["sport"]
        user <- map["user"]
        start_time <- map["start_time"]
        title <- map["title"]
        location <- map["location"]
        private_state <- map["private_state"]
        count_accepted <- map["count_accepted"]
        count_invited <- map["count_invited"]
        count_maybe <- map["count_maybe"]
        repeat_type <- map["repeat_type"]
    }
}


