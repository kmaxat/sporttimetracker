//
//  GroupListTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 10.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import Nuke
import PKHUD

class GroupListTC: UITableViewController, ClearSelectionProtocol {

    var data = [DataGT]()
    let disposeBag = DisposeBag()
    let fmt = DateFormatter()
    var days = [String]()
    var rootVC: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(self.getData), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!)
        
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.rowHeight = UITableViewAutomaticDimension
        
        days = fmt.shortWeekdaySymbols
    }
    
    func getData(){
        HUD.show(.progress)
        Network.request(target: .getMyGroupTrainings())
            .mapObject(GroupTrainings.self)
            .subscribe(onNext: { (groupTrainings) in
                self.data = groupTrainings.data ?? []
                self.tableView.reloadData()
                }, onError: { (error) in
                    HUD.hide()
                    self.refreshControl?.endRefreshing()
                }, onCompleted: { 
                    HUD.hide()
                    self.refreshControl?.endRefreshing()
                }, onDisposed: nil).addDisposableTo(disposeBag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearTC(self)
        getData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                                               for: indexPath) as! GroupTrainingCell
        let training = self.data[(indexPath as NSIndexPath).row]
        cell.txtSport.text = training.sport.title
        cell.txtDate.text = to_show_stringFromDateWithTime(training.start_date, start_time: training.start_time)
        
        if training.repeat_days != nil {
            let formattedDays = training.repeat_days!.map {
                self.days[$0]
            }
            cell.txtRepeat.text = formattedDays.joined(separator: ", ")
        } else {
            cell.txtRepeat.text = ""
        }
        
        cell.sportImg.image = nil
        Nuke.loadImage(with: URL(string: training.sport.picture)!, into: cell.sportImg)
        cell.txtCount.text = "\(training.count_accepted)"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let training = self.data[(indexPath as NSIndexPath).row]
        let infoVC = st.instantiateViewController(withIdentifier: "TrainingInfo") as! TrainingDetailVC
        infoVC.trainingID = training.id
        self.rootVC?.navigationController?.pushViewController(infoVC, animated: true)
    }
}
