
//
//  TrainingCreateTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 16.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Nuke
import PKHUD

class TrainingCreateTC: UITableViewController, ClearSelectionProtocol {
    
    var viewModel: CreateTrainingViewModel!
    @IBOutlet weak var checkSportWidth: NSLayoutConstraint!
    @IBOutlet weak var checkPlaceWidth: NSLayoutConstraint!
    @IBOutlet weak var checkNameWidth: NSLayoutConstraint!
    @IBOutlet weak var checkPrivateStateWidth: NSLayoutConstraint!
    @IBOutlet weak var checkInviteFriendsWidth: NSLayoutConstraint!
    @IBOutlet weak var checkDateWidth: NSLayoutConstraint!
    @IBOutlet weak var checkRepeatWidth: NSLayoutConstraint!
    @IBOutlet weak var imgSport: UIImageView!
    @IBOutlet weak var txtSport: UILabel!
    @IBOutlet weak var txtPlace: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtIsOpen: UILabel!
    @IBOutlet weak var txtDate: UILabel!
    @IBOutlet weak var txtFriends: UILabel!
    @IBOutlet weak var txtRepeat: UILabel!
    @IBOutlet weak var txtInfoTV: UITextView!
    @IBOutlet weak var privateSwitch: UISwitch!
    @IBOutlet weak var doneBtn: UIButton!
    var training: DataGT?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CreateTrainingViewModel()
        hideChecks()
        fillView()
        binds()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearTC(self)
    }
    
    func binds() {
        txtInfoTV?.rx.text
            .bindNext({ (str) in
                let currentOffset = self.tableView.contentOffset
                UIView.setAnimationsEnabled(false)
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
                UIView.setAnimationsEnabled(true)
                self.tableView.setContentOffset(currentOffset, animated: false)
                if str?.isEmpty == false {
                    self.viewModel.info = str
                }
            }).addDisposableTo(rx_disposeBag)
        
        txtName?.rx.text
            .bindNext({ (str) in
                if str?.isEmpty == false {
                    self.checkNameWidth.constant = 30
                } else {
                    self.checkNameWidth.constant = 0
                }
                self.viewModel.title = str
            }).addDisposableTo(rx_disposeBag)
        
        privateSwitch?.rx.value
            .bindNext({ (open) in
                self.viewModel.isOpen = open
            }).addDisposableTo(rx_disposeBag)
    }
    
    func hideChecks() {
        self.checkSportWidth.constant = 0
        self.checkPlaceWidth.constant = 0
        self.checkNameWidth.constant = 0
        self.checkPrivateStateWidth.constant = 0
        self.checkInviteFriendsWidth.constant = 0
        self.checkDateWidth.constant = 0
        self.checkRepeatWidth.constant = 0
    }
    
    func fillView() {
        if training != nil {
            Nuke.loadImage(with: URL(string: training!.sport.picture)!, into: self.imgSport)
            let date = to_show_stringFromDateWithTime(training!.start_date, start_time: training!.start_time)
            txtDate.text = date
            privateSwitch.setOn(training?.private_state == "friends" ? true : false, animated: true)
            txtName.text = training!.title
            txtSport.text = training!.sport.title
            txtFriends.text = "\(training!.count_invited) человек"
            txtPlace.text = "Выбрано"
            
            let fmt = DateFormatter()
            let days = fmt.shortWeekdaySymbols
            let formattedDays = training!.repeat_days?.map {
                days![$0]
            }
            
            if formattedDays != nil {
                viewModel.repeatDays = training!.repeat_days
                txtRepeat.text = formattedDays!.joined(separator: ", ")
            } else {
                txtRepeat.text = "Повторять"
                viewModel.repeatDays = nil
            }
            
            viewModel.date = dateFromStartDateAndTime(training!.start_date, start_time: training!.start_time)
            viewModel.isOpen = training?.private_state == "friends" ? true : false
            viewModel.title = training!.title
            viewModel.sport = training!.sport
            viewModel.place = training!.location
            viewModel.training_id = training!.id
            
            doneBtn.setTitle("Изменить", for: UIControlState())
            let put = self.viewModel.putTraining
            doneBtn!.rx.tap.subscribe { (btn) in
                HUD.show(.progress)
                put().subscribe(onNext: { (didPost) in
                    HUD.hide()
                    if didPost == true {
                        _ = self.navigationController?.popViewController(animated: true)
                    } else {
                        showAlert("SportTime", message: "Спорт, место, название и дата обязательны", controllder: self)
                    }
                }).addDisposableTo(self.rx_disposeBag)
                }.addDisposableTo(rx_disposeBag)
        } else {
            let post = self.viewModel.postTraining
            doneBtn!.rx.tap.subscribe { (event) in
                HUD.show(.progress)
                post().subscribe(onNext: { (didPost) in
                    HUD.hide()
                    if didPost == true {
                        _ = self.navigationController?.popViewController(animated: true)
                    } else {
                        showAlert("SportTime", message: "Спорт, место, название и дата обязательны", controllder: self)
                    }
                }).addDisposableTo(self.rx_disposeBag)
                }.addDisposableTo(rx_disposeBag)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sport" {
            let vc = segue.destination as! ChooseSportTC
            vc.delegate = self
            vc.viewModel = viewModel
        } else if segue.identifier == "place" {
            let vc = segue.destination as! ChoosePlaceVC
            vc.delegate = self
            vc.viewModel = viewModel
        } else if segue.identifier == "friends" {
            let vc = segue.destination as! ChooseFriendsTC
            vc.delegate = self
            vc.viewModel = viewModel
        } else if segue.identifier == "repeat" {
            let vc = segue.destination as! ChooseRepeatTC
            vc.delegate = self
            vc.viewModel = viewModel
        }
    }
}

extension TrainingCreateTC {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).row == 5 {
            DatePickerDialog().show("Дата и время тренировки", doneButtonTitle: "Готово", cancelButtonTitle: "Отмена", datePickerMode: .dateAndTime) {
                (date) -> Void in
                let formatter = DateFormatter()
                formatter.dateFormat = "EEEE, dd MMMM yyyy HH:mm"
                self.txtDate!.text = formatter.string(from: date)
                self.viewModel.date = date
                self.checkDateWidth.constant = 30
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 7 {
            return UITableViewAutomaticDimension
        } else {
            return 65
        }
    }
}

extension TrainingCreateTC: ChooseSportDelegate, ChooseFriendsDelegate, ChooseRepeatDelegate, ChoosePlaceDelegate {
    func didiChooseSport(_ sport: dataSport) {
        self.txtSport?.text = sport.title
        Nuke.loadImage(with: URL(string: sport.picture)!, into: self.imgSport)
        self.viewModel.sport = sport
        self.checkSportWidth.constant = 30
    }
    
    func didiChooseFriends(_ friends: [Int]) {
        self.txtFriends?.text = "\(friends.count) человек"
        self.viewModel.friendsID = friends.count > 0 ? friends : nil
        self.checkInviteFriendsWidth.constant = 30
    }
    
    func didiChooseRepeat(_ repeatDays: [Int]) {
        if repeatDays.count == 0 {
            self.viewModel.repeatDays = nil
            self.checkRepeatWidth.constant = 0
            self.txtRepeat?.text = "Повторять"
            return
        }
        
        let fmt = DateFormatter()
        let days = fmt.shortWeekdaySymbols
        let formattedDays = repeatDays.map{
            days![$0]
        }
        self.txtRepeat?.text = formattedDays.count > 0 ? formattedDays.joined(separator: ", ") : "Повторять"
        self.viewModel.repeatDays = repeatDays
        self.checkRepeatWidth.constant = 30
    }
    
    func didiChoosePlace(_ loc: LocationObject) {
        self.txtPlace?.text = "Выбрано"
        self.viewModel.place = loc
        self.checkPlaceWidth.constant = 30
    }
}

