//
//  ChooseFriendsTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 16.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import PKHUD
import Nuke

protocol ChooseFriendsDelegate {
    func didiChooseFriends(_ friends: [Int])
}

class ChooseFriendsTC: UITableViewController {

    var viewModel: CreateTrainingViewModel!
    
    var data = [DataFriends]()
    let bag = DisposeBag()
    
    var selectedFriends = [Int]()
    
    var delegate: ChooseFriendsDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.allowsMultipleSelection = true
        
        HUD.show(.progress)
        Network.request(target: .getFriends())
            .mapObject(Friends.self)
            .subscribe(onNext: { (friends) in
                self.data = friends.data ?? []
                self.tableView.reloadData()
                }, onError: { (error) in
                    HUD.hide()
                }, onCompleted: { 
                    HUD.hide()
                }, onDisposed: nil).addDisposableTo(bag)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.didiChooseFriends(self.selectedFriends)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                                               for: indexPath) as! UserCell
        let friend = data[indexPath.row]
        let friendID = data[indexPath.row].id
        
        if selectedFriends.contains(friendID) == false {
            cell.accessoryType = .none
        } else {
            cell.accessoryType = .checkmark
        }
        
        cell.txt.text = friend.first_name + " " + friend.last_name
        cell.img.image = nil
        if let url = URL(string: friend.picture ?? "") {
            Nuke.loadImage(with: url, into: cell.img)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedFriends.append(data[(indexPath as NSIndexPath).row].id)
        tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.checkmark
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.selectedFriends.removeObject(data[(indexPath as NSIndexPath).row].id)
        tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.none
    }

}
