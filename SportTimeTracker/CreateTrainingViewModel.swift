//
//  CreateTrainingViewModel.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 16.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import RxSwift
import PKHUD

class CreateTrainingViewModel {
    
    var sport: dataSport?
    var place: LocationObject?
    var title: String?
    var isOpen = true
    var friendsID: [Int]?
    var date: Date?
    var repeatDays: [Int]?
    var info: String?
    var training_id: Int?
    let bag = DisposeBag()
    
    func postTraining() -> Observable<Bool> {
        if checkRules() == false {
            return Observable.create { observer in
                observer.on(.next(false))
                observer.on(.completed)
                return Disposables.create()
            }
        }
        
        let strDate = (self.date! as NSDate).formattedDate(withFormat: "yyyy-MM-d")
        let strTime = (self.date! as NSDate).formattedDate(withFormat: "HH:mm")
        let sportID = self.sport?.id
        let title = self.title
        let location = self.place
        let privateState = self.isOpen == true ? "friends" : "close"
        let description = self.info
        var repeatType = "none"
        if self.repeatDays != nil {
            repeatType = "week"
        }
        let invitedUsers = self.friendsID
        let repeatDays = self.repeatDays
        
        return Observable.create { observer in
            HUD.show(.progress)
            Network.request(target: .addGroupTraining(sport_id: sportID!, title: title, location: location, private_state: privateState, start_date: strDate!, start_time: strTime!, description: description, repeat_type: repeatType, invites_user: invitedUsers, repeat_days: repeatDays))
                .subscribe(onNext: { (response) in
                    if response.statusCode != 200 {
                        observer.on(.next(false))
                    } else {
                        observer.on(.next(true))
                        observer.on(.completed)
                    }
                    }, onError: { (error) in
                        HUD.hide()
                        observer.on(.next(false))
                        observer.on(.error(error))
                    }, onCompleted: { 
                        HUD.hide()
                    }, onDisposed: nil).addDisposableTo(self.bag)
            return Disposables.create()
        }
    }
    
    func putTraining() -> Observable<Bool> {
        
        if checkRules() == false {
            return Observable.create { observer in
                observer.on(.next(false))
                observer.on(.completed)
                return Disposables.create()
            }
        }
        
        let strDate = (self.date! as NSDate).formattedDate(withFormat: "yyyy-MM-d")
        let strTime = (self.date! as NSDate).formattedDate(withFormat: "HH:mm")
        let trainingID = self.training_id
        let sportID = self.sport?.id
        let title = self.title
        let location = self.place
        let privateState = self.isOpen == true ? "friends" : "close"
        let description = self.info
        var repeatType = "none"
        if self.repeatDays != nil {
            repeatType = "week"
        }
        let invitedUsers = self.friendsID
        let repeatDays = self.repeatDays
        
        return Observable.create { observer in
            HUD.show(.progress)
            Network.request(target: .putGroupTraining(training_id: trainingID!, sport_id: sportID, title: title, location: location, private_state: privateState, start_date: strDate, start_time: strTime, description: description, repeat_type: repeatType, invites_user: invitedUsers, repeat_days: repeatDays))
                .subscribe(onNext: { (response) in
                    print(response.statusCode)
                    if response.statusCode != 200 {
                        observer.on(.next(false))
                    } else {
                        observer.on(.next(true))
                    }
                    }, onError: { (error) in
                        HUD.hide()
                        observer.on(.next(false))
                        observer.on(.error(error))
                        return
                    }, onCompleted: {
                        HUD.hide()
                        observer.on(.completed)
                    }, onDisposed: nil).addDisposableTo(self.bag)
            return Disposables.create()
        }
    }
    
    func checkRules() -> Bool {
        let sportRule = self.sport != nil
        let placeRule = self.place != nil
        let titleRule = self.title != nil
        let dateRule = self.date != nil
        return sportRule && placeRule && titleRule && dateRule
    }
}
