//
//  ForgotPassVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 09.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift

class ForgotPassVC: UIViewController, ClearNavBarProtocol, UITextFieldDelegate {
    
    @IBOutlet fileprivate weak var txtEmail: UITextField?
    @IBOutlet fileprivate weak var rememberBtn: UIButton?
    
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        clearNavBar(self.navigationController!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.txtEmail!:
            rememberPass()
            break
        default:
            break
        }
        return true
    }
    
    func rememberPass() {
        Network.request(target: .forgotPass(email: (txtEmail?.text)!))
            .subscribe(onNext: { (response) in
                print(response.statusCode)
                if response.statusCode < 300 {
                    showAlert("SportTime", message: "Email с инструкциями отправлен", controllder: self)
                } else {
                    showErrorAlertWithCode(code: response.statusCode, controller: self)
                }
                }, onError: { (error) in
                    showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                }, onCompleted: {}, onDisposed: {}).addDisposableTo(bag)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.view.frame.origin.y < 0 {
            animateViewMoving(false, moveValue: 100)
        }
    }
    
    func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
}
