//
//  FriendsVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 10.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import PageMenu
import RxSwift
import PKHUD

class FriendsVC: UIViewController {
    
    var pageMenu : CAPSPageMenu?
    var searchController: UISearchController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNavigationBarItem()
        
        let friendsLent = st.instantiateViewController(withIdentifier: "friendsLent") as! FriendsLentTC
        friendsLent.title = "Лента"
        friendsLent.rootVC = self
        
        let friendsList = st.instantiateViewController(withIdentifier: "friendsList") as! FriendsListTC
        friendsList.title = "Друзья"
        friendsList.rootVC = self
        
        let controllerArray = [friendsLent, friendsList]
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(Constants.colors.appMainColor),
            .menuItemSeparatorPercentageHeight(0.0),
            .useMenuLikeSegmentedControl(true)
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
        
        setupSearchBar()
    }

    func setupSearchBar() {
        let src = st.instantiateViewController(withIdentifier: "friendsSearch") as! FriendsSearch
        src.rootVC = self
        searchController = UISearchController(searchResultsController: src)
        searchController.searchResultsUpdater = src
        self.searchController.delegate = src
        self.searchController.searchBar.delegate = src
        searchController.dimsBackgroundDuringPresentation = true
        self.searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.titleView = searchController.searchBar
        src.searchController = self.searchController
        definesPresentationContext = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}

