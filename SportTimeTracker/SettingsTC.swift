//
//  SettingsTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 10.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

class SettingsTC: UITableViewController, ClearSelectionProtocol {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Настройки"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearTC(self)
        self.setNavigationBarItem()
    }
}
