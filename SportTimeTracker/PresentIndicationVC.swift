//
//  PresentIndicationVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 19.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import MapKit

class PresentIndicationVC: UITableViewController, ChooseIndicatorDelegate {
    
    @IBOutlet fileprivate weak var pinBtn: UIButton!
    @IBOutlet weak var txtFirstIndicatorTitle: UILabel!
    @IBOutlet weak var txtFirstIndicator: UILabel!
    @IBOutlet weak var txtSecondIndicatorTitle: UILabel!
    @IBOutlet weak var txtSecondIndicator: UILabel!
    @IBOutlet weak var txtThirdIndicatorTitle: UILabel!
    @IBOutlet weak var txtThirdIndicator: UILabel!
    @IBOutlet weak var txtFourthIndicatorTitle: UILabel!
    @IBOutlet weak var txtFourthIndicator: UILabel!
    var firstIndicator: Indicator?
    var secondIndicator: Indicator?
    var thirdIndicator: Indicator?
    var fourthIndicator: Indicator?
    var timer: Observable<NSInteger>!
    let signalCollector = SignalCollector.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstIndicator = .distance
        secondIndicator = .time
        thirdIndicator = .avgSpeed
        fourthIndicator = .pace
        
        binds()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let location = signalCollector.calculator.locations.last {
            updateIndicators(location)
        }
    }
    
    func binds() {
        pinBtn?.rx.tap
            .bindNext({
                _ = self.navigationController?.popViewController(animated: true)
            }).addDisposableTo(rx_disposeBag)
        
        signalCollector.location.asObservable().subscribe { (location) in
            if location.element!.coordinate.latitude > 0 {
                self.updateIndicators(location.element!)
            }
            }.addDisposableTo(rx_disposeBag)
        
        self.timer.subscribe(onNext: { sec -> Void in
            self.signalCollector.calculator.seconds = sec
            
            switch self.firstIndicator! {
            case .time:
                self.txtFirstIndicator?.text = stringFromTimeInterval(sec: self.signalCollector.calculator.seconds)
            default:
                break
            }
            
            switch self.secondIndicator! {
            case .time:
                self.txtSecondIndicator?.text = stringFromTimeInterval(sec: self.signalCollector.calculator.seconds)
            default:
                break
            }
            
            switch self.thirdIndicator! {
            case .time:
                self.txtThirdIndicator?.text = stringFromTimeInterval(sec: self.signalCollector.calculator.seconds)
            default:
                break
            }
            
            switch self.fourthIndicator! {
            case .time:
                self.txtFourthIndicator?.text = stringFromTimeInterval(sec: self.signalCollector.calculator.seconds)
            default:
                break
            }
            
        }).addDisposableTo(rx_disposeBag)
    }
    
    func updateIndicators(_ location: CLLocation) {
        let distance = stringKM(signalCollector.calculator._distance())
        let speed = stringSpeed(signalCollector.calculator.speed)
        let pace = stringPaceFromTimeInterval(sec: Int(signalCollector.calculator._pace()))
        let avgSpeed = stringSpeed(signalCollector.calculator._avgSpeed())
        let avgPace = stringPaceFromTimeInterval(sec: Int(signalCollector.calculator._avgPace()))
        
        switch self.firstIndicator! {
        case .distance:
            self.txtFirstIndicator.text = distance
        case .time:
            break
        case .speed:
            self.txtFirstIndicator.text = speed
        case .pace:
            self.txtFirstIndicator.text = pace
        case .avgSpeed:
            self.txtFirstIndicator.text = avgSpeed
        case .avgPace:
            self.txtFirstIndicator.text = avgPace
        }
        
        switch self.secondIndicator! {
        case .distance:
            self.txtSecondIndicator.text = distance
        case .time:
            break
        case .speed:
            self.txtSecondIndicator.text = speed
        case .pace:
            self.txtSecondIndicator.text = pace
        case .avgSpeed:
            self.txtSecondIndicator.text = avgSpeed
        case .avgPace:
            self.txtSecondIndicator.text = avgPace
        }
        
        switch self.thirdIndicator! {
        case .distance:
            self.txtThirdIndicator.text = distance
        case .time:
            break
        case .speed:
            self.txtThirdIndicator.text = speed
        case .pace:
            self.txtThirdIndicator.text = pace
        case .avgSpeed:
            self.txtThirdIndicator.text = avgSpeed
        case .avgPace:
            self.txtThirdIndicator.text = avgPace
        }
        
        switch self.fourthIndicator! {
        case .distance:
            self.txtFourthIndicator.text = distance
        case .time:
            break
        case .speed:
            self.txtFourthIndicator.text = speed
        case .pace:
            self.txtFourthIndicator.text = pace
        case .avgSpeed:
            self.txtFourthIndicator.text = avgSpeed
        case .avgPace:
            self.txtFourthIndicator.text = avgPace
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.height/5
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chooseIndicatorVC = st.instantiateViewController(withIdentifier: "indicators") as? SelectIndicatorVC
        chooseIndicatorVC!.delegate = self
        chooseIndicatorVC?.indicatorPlace = (indexPath as NSIndexPath).row
        self.navigationController?.show(chooseIndicatorVC!, sender: self)
    }
    
    func didiChooseIndicator(_ indicator: Indicator, name: String, indicatorPlace: Int) {
        switch indicatorPlace {
        case 0:
            txtFirstIndicatorTitle.text = name
            txtFirstIndicator.text = ""
            firstIndicator = indicator
        case 1:
            txtSecondIndicatorTitle.text = name
            txtSecondIndicator.text = ""
            secondIndicator = indicator
        case 2:
            txtThirdIndicatorTitle.text = name
            txtThirdIndicator.text = ""
            thirdIndicator = indicator
        case 3:
            txtFourthIndicatorTitle.text = name
            txtFourthIndicator.text = ""
            fourthIndicator = indicator
        default:
            break
        }
    }
}
