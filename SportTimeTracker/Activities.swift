//
//  Activities.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper

enum actionActivities: String, RawRepresentable {
    case training_finished// = "training_finished"
    case group_training_created// = "group_training_created"
    case invitation_accepted// = "invitation_accepted"
    case became_friends// = "became_friends"
}

class Activities : Mappable {
    
    var meta: Meta?

    var data = [DataActivities]()
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        meta <- map["meta"]
        data <- map["data"]
    }
}

class DataActivities: Mappable {

    var date = String()
    
    var id: Int = 0
    
    var user = User()
    
    var friend = User()
    
    var group_training = DataGT()
    
    var training = DataTR()
    
    var action: actionActivities!
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        user <- map["user"]
        training <- map["training"]
        group_training <- map["group_training"]
        action <- map["action"]
        date <- map["date"]
        friend <- map["friend"]
    }
}
