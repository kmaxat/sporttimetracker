//
//  InboxTrainingCell.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 27.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift

class InboxTrainingInviteCell: UITableViewCell {
    
    var training: DataGT!
    var disposeBagCell:DisposeBag = DisposeBag()
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var txtText: UILabel!
    @IBOutlet weak var btnOptions: UIButton!
    
    override func prepareForReuse() {
        disposeBagCell = DisposeBag()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
