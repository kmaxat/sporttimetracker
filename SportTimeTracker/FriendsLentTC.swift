//
//  FriendsLentTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 10.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import Nuke
import PKHUD

class FriendsLentTC: UITableViewController, ClearSelectionProtocol {

    var data = [DataActivities]()
    var rootVC: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.rowHeight = UITableViewAutomaticDimension
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(self.getData), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!)
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearTC(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func getData() {
        HUD.show(.progress)
        Network.request(target: .activities())
        .mapObject(Activities.self)
        .subscribe(onNext: { (activities) in
            self.data = activities.data
            self.tableView.reloadData()
            }, onError: { (error) in
                HUD.hide()
                self.refreshControl?.endRefreshing()
            }, onCompleted: { 
                HUD.hide()
                self.refreshControl?.endRefreshing()
            }, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                                               for: indexPath) as! ActivityCel
        
        let activity = self.data[(indexPath as NSIndexPath).row]
        cell.img.image = nil
        if let url = activity.user.picture {
            Nuke.loadImage(with: URL(string: url)!, into: cell.img)
        } else {
            cell.img.image = #imageLiteral(resourceName: "avatar_placeholder")
        }
        cell.txtFirst.text = (dateFromString(activity.date) as NSDate).formattedDate(with: .long)
        var text = ""
        switch activity.action! {
        case .training_finished:
            let genderText = activity.user.gender == "male" ? "закончил тренировку" : "закончила тренировку"
            text = activity.user.first_name + " " + activity.user.last_name + " " + genderText + " за \(stringFromTimeInterval(sec: activity.training.duration)) пройдя дистанцию в \(activity.training.distance)"
        case .group_training_created:
            let genderText = activity.user.gender == "male" ? "создал тренировку" : "создала тренировку"
            text = activity.user.first_name + " " + activity.user.last_name + " " + genderText + " " + activity.group_training.title
        case .invitation_accepted:
            let genderText = activity.user.gender == "male" ? "принял участие в тренировке" : "приняла участие в тренировке"
            text = activity.user.first_name + " " + activity.user.last_name + " " + genderText + " " + activity.group_training.title
        case .became_friends:
            text = activity.user.first_name + " " + activity.user.last_name + " и " + activity.friend.first_name + " " + activity.friend.last_name + " стали друзьями"
        }
        
        cell.txtSecond.text = text
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let activity = self.data[(indexPath as NSIndexPath).row]
        
        switch activity.action! {
        case .became_friends:
            let profileVC = st.instantiateViewController(withIdentifier: "User") as! UserProfileVC
            profileVC.userID = activity.friend.id
            profileVC.hideCloseBtn = true
            self.rootVC?.navigationController?.pushViewController(profileVC, animated: true)
        case .training_finished:
            let detailTrainingVC = st.instantiateViewController(withIdentifier: "RouteInfo") as! RouteDetailVC
            detailTrainingVC.routeID = activity.training.id
            self.rootVC?.navigationController?.pushViewController(detailTrainingVC, animated: true)
        default:
            let infoVC = st.instantiateViewController(withIdentifier: "TrainingInfo") as! TrainingDetailVC
            infoVC.trainingID = activity.group_training.id
            self.rootVC?.navigationController?.pushViewController(infoVC, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

}
