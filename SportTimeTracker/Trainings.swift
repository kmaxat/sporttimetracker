//
//  Trainings.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper
import MapKit

class Trainings: Mappable {

    var meta: Meta?

    var data: [DataTR]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        meta <- map["meta"]
        data <- map["data"]
    }
}


class DataTR: Mappable {

    var id: Int = 0

    var average_speed: Double = 0

    var calories: Double = 0

    var max_speed: Double = 0

    var user_id: Int = 0

    var average_pace: Double = 0

    var notes = String()

    var date = String()

    var route = [LocationObject]()

    var route2d = [CLLocationCoordinate2D]()
    
    var locations = [CLLocation]()

    var duration: Int = 0

    var sport = dataSport()

    var distance: Double = 0

    var max_pace: Double = 0
    
    init () {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        average_speed <- map["average_speed"]
        calories <- map["calories"]
        max_speed <- map["max_speed"]
        user_id <- map["user_id"]
        average_pace <- map["average_pace"]
        notes <- map["notes"]
        date <- map["date"]
        route <- map["route"]
        duration <- map["duration"]
        sport <- map["sport"]
        distance <- map["distance"]
        max_pace <- map["max_pace"]
    }
}

