//
//  GroupMap.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 10.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import Nuke
import PKHUD

class GroupMap: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet fileprivate weak var map: MKMapView!
    var rootVC: UIViewController?
    var locationManager = CLLocationManager()
    var location: LocationObject!
    var data = [DataGT]()
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        map.delegate = self
        self.location = LocationObject()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    func getDataWithDate(_ date: String) {
        HUD.show(.progress)
        Network.request(target: .getGroupTrainings(location: self.location, date: date))
            .mapObject(GroupTrainings.self)
            .subscribe(onNext: { (trainings) in
                if trainings.data != nil {
                    self.data = trainings.data ?? []
                    self.dropPins()
                }
                }, onError: { (error) in
                    HUD.hide()
                }, onCompleted: { 
                    HUD.hide()
                }, onDisposed: nil).addDisposableTo(bag)
    }
    
    func getData() {
        HUD.show(.progress)
        Network.request(target: .getGroupTrainings(location: self.location, date: ""))
        .mapObject(GroupTrainings.self)
        .subscribe(onNext: { (trainings) in
            if trainings.data != nil {
                self.data = trainings.data ?? []
                self.dropPins()
            }
            }, onError: { (error) in
                HUD.hide()
            }, onCompleted: { 
                HUD.hide()
            }, onDisposed: nil).addDisposableTo(bag)
    }
    
    func dropPins() {
        if self.map!.annotations.count > 0 {
            self.map!.removeAnnotations(self.map!.annotations)
        }
        
        for training in self.data {
            let coordinate = CLLocationCoordinate2D(latitude: training.location.lat, longitude: training.location.lon)
            let annotation = Annotation(title: "\(training.count_accepted) идут", locationName: "\(training.sport.title)", coordinate: coordinate, training: training)
            self.map.addAnnotation(annotation)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        locationManager.stopUpdatingLocation()
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        var mapRegion = MKCoordinateRegion()
        mapRegion.center = map.userLocation.coordinate
        mapRegion.span.latitudeDelta = 0.05
        mapRegion.span.longitudeDelta = 0.05
        
        location.lat = map.userLocation.coordinate.latitude
        location.lon = map.userLocation.coordinate.longitude
        
        self.getData()
        
        map.setRegion(mapRegion, animated: true)
        
        locationManager.stopUpdatingLocation()
        mapView.showsUserLocation = false
    }

    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Annotation {
            let identifier = "pin"
            var view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.animatesDrop = true
                view.pinTintColor = .red
                view.canShowCallout = true
                let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                btn.setImage(UIImage(named: "arrow"), for: UIControlState())
                view.rightCalloutAccessoryView = btn as UIView
                let imgView = UIImageView()
                imgView.image = nil
                Nuke.loadImage(with: URL(string: annotation.training.sport.picture)!, into: imgView)
                imgView.contentMode = .scaleAspectFit
                imgView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
                view.leftCalloutAccessoryView = imgView
            }
            return view
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
            calloutAccessoryControlTapped control: UIControl) {
        let annotation = view.annotation as! Annotation
        let infoVC = st.instantiateViewController(withIdentifier: "TrainingInfo") as! TrainingDetailVC
        infoVC.trainingID = annotation.training.id
        rootVC?.navigationController?.pushViewController(infoVC, animated: true)
    }
}
