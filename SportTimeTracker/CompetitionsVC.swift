//
//  CompetitionsVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 10.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import PageMenu

class CompetitionsVC: UIViewController {
    
    var pageMenu : CAPSPageMenu?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let compMy = st.instantiateViewController(withIdentifier: "compMy") as! MyCompTC
        compMy.rootVC = self
        compMy.title = "Мои маршруты"
        
        let compNear = st.instantiateViewController(withIdentifier: "compNear") as! NearCompTC
        compNear.rootVC = self
        compNear.title = "Маршруты рядом"
        
        let controllerArray = [compMy, compNear]
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(Constants.colors.appMainColor),
            .menuItemSeparatorPercentageHeight(0.0),
            .useMenuLikeSegmentedControl(true)
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

}
