//
//  ChooseRepeatTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 16.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

protocol ChooseRepeatDelegate {
    func didiChooseRepeat(_ repeatDays: [Int])
}

class ChooseRepeatTC: UITableViewController {
    
    var viewModel: CreateTrainingViewModel!
    var data = [String]()
    var selectedIndexes = [IndexPath]()
    var selectedDays = [Int]()
    var delegate: ChooseRepeatDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        let fmt = DateFormatter()
        self.data = fmt.weekdaySymbols
        self.selectedDays = viewModel.repeatDays ?? []
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let selectedDays = selectedIndexes.map {
            ($0 as NSIndexPath).row
        }
        delegate?.didiChooseRepeat(selectedDays)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                                               for: indexPath)
        cell.textLabel?.text = data[(indexPath as NSIndexPath).row]

        if selectedDays.contains(indexPath.row) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedDays.contains((indexPath as NSIndexPath).row) {
            self.selectedIndexes.removeObject(indexPath)
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.none
        } else {
            self.selectedIndexes.append(indexPath)
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.checkmark
        }
    }
}
