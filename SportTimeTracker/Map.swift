//
//  ExtensionMap.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 17/10/2016.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import Async

extension MKMapView {
    func addPolygon(points: [CLLocationCoordinate2D]) {
        var points = points
//        let myPolyline = MKPolyline(coordinates: &points, count: points.count)
//        self.add(myPolyline)
        let routeOverlay = MKPolygon(coordinates: &points, count: points.count)
        self.add(routeOverlay)
        Async.main(after: 1) {
            self.setVisibleMapRect(routeOverlay.boundingMapRect, edgePadding: UIEdgeInsets(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0), animated: true)
        }
    }
    
    func lineView(rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        let lineView = MKPolylineRenderer(overlay: overlay)
        lineView.lineWidth = 3.0
        lineView.strokeColor = UIColor(hex: "0000ff")
        return lineView
    }
}
