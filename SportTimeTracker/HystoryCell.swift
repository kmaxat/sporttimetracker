//
//  HystoryCell.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 22.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

class HystoryCell: UITableViewCell {

    @IBOutlet weak var imgSport: UIImageView!
    @IBOutlet weak var txtDistance: UILabel!
    @IBOutlet weak var txtTime: UILabel!
    @IBOutlet weak var txtCalories: UILabel!
    @IBOutlet weak var txtDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
