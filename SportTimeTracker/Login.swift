//
//  Login.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper

class Login: Mappable {

    var email: String?

    var password: String?
    
    required init?(map: Map){
        
    }
 
    func mapping(map: Map) {
        email <- map["email"]
        password <- map["password"]
    }
}
