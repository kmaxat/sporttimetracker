//
//  TrainingCommonController.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 17/10/2016.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import AURUnlockSlider
import Nuke
import RxSwift

extension TrainingVC {
    
    func binds() {
        let isRunning = Observable.from([self.startBtn!.rx.tap.map({_ in self.sport == nil ? false : true}), self.pauseBtn!.rx.tap.map({_ in false}), self.stopBtn!.rx.tap.map({_ in false})])
            .merge()
            .startWith(false)
            .shareReplayLatestWhileConnected()
        
        let isntRunning = isRunning.map({running in !running}).shareReplay(1)
        isRunning.bindTo(self.startBtn!.rx.isHidden).addDisposableTo(rx_disposeBag)
        isRunning.bindTo(self.pauseBtn!.rx.isEnabled).addDisposableTo(rx_disposeBag)
        isntRunning.bindTo(self.startBtn!.rx.isEnabled).addDisposableTo(rx_disposeBag)
        isntRunning.bindTo(self.stopBtn!.rx.isHidden).addDisposableTo(rx_disposeBag)
        isntRunning.bindTo(self.pauseBtn!.rx.isHidden).addDisposableTo(rx_disposeBag)
        
        self.timer = Observable<NSInteger>.interval(1, scheduler: MainScheduler.instance)
            .withLatestFrom(isRunning, resultSelector: {_, running in running})
            .filter({running in running})
            .scan(0, accumulator: {(acc, _) in
                if self.trainingStopped == true {
                    self.trainingStopped = false
                    return 0
                } else {
                    return acc + 1
                }
            })
            .startWith(0)
            .shareReplayLatestWhileConnected()
        
        timerBtn.rx.tap
            .bindNext{ [weak self] in
                if self?.timer == nil {
                    showAlert("SportTime", message: "Начните тренировку", controllder: self!)
                    return
                }
                let indicationVC = st.instantiateViewController(withIdentifier: "indication") as? PresentIndicationVC
                indicationVC?.timer = self?.timer
                self?.navigationController?.show(indicationVC!, sender: self)
            }.addDisposableTo(rx_disposeBag)
        
        sportBtn.rx.tap
            .bindNext{ [weak self] in
                let chooseSportVC = st.instantiateViewController(withIdentifier: "chooseSport") as? ChooseSportTC
                chooseSportVC!.delegate = self
                self?.navigationController?.show(chooseSportVC!, sender: self)
            }.addDisposableTo(rx_disposeBag)
        
        firstIndBtn.rx.tap
            .bindNext{ [weak self] in
                let chooseIndicatorVC = st.instantiateViewController(withIdentifier: "indicators") as? SelectIndicatorVC
                chooseIndicatorVC!.delegate = self
                chooseIndicatorVC?.indicatorPlace = 0
                self?.navigationController?.show(chooseIndicatorVC!, sender: self)
            }.addDisposableTo(rx_disposeBag)
        
        secondIndBtn.rx.tap
            .bindNext{ [weak self] in
                let chooseIndicatorVC = st.instantiateViewController(withIdentifier: "indicators") as? SelectIndicatorVC
                chooseIndicatorVC!.delegate = self
                chooseIndicatorVC?.indicatorPlace = 1
                self?.navigationController?.show(chooseIndicatorVC!, sender: self)
            }.addDisposableTo(rx_disposeBag)
        
        signalCollector.location.subscribe {
            self.updateView($0.element!)
            }.addDisposableTo(rx_disposeBag)
        
        self.timer.subscribe(onNext: { msecs -> Void in
            self.signalCollector.time = msecs
            switch self.firstIndicator! {
            case .time:
                self.txtFirstIndication.text = stringFromTimeInterval(sec: self.signalCollector.calculator.seconds)
            default:
                break
            }
            
            switch self.secondIndicator! {
            case .time:
                self.txtSecondIndication?.text = stringFromTimeInterval(sec: self.signalCollector.calculator.seconds)
            default:
                break
            }
        }).addDisposableTo(rx_disposeBag)
    }
}

extension TrainingVC: ChooseSportDelegate, ChooseIndicatorDelegate, AURUnlockSliderDelegate {
    func didiChooseSport(_ sport: dataSport) {
        self.sport = sport
        self.txtSport.text = sport.title
        Nuke.loadImage(with: URL(string: sport.picture)!, into: self.imgSport)
    }
    
    func didiChooseIndicator(_ indicator: Indicator, name: String, indicatorPlace: Int) {
        switch indicatorPlace {
        case 0:
            self.txtFirstIndicator?.text = name
            self.txtFirstIndication.text = ""
            firstIndicator = indicator
        case 1:
            self.txtSecondIndicator?.text = name
            self.txtSecondIndication?.text = ""
            secondIndicator = indicator
        default:
            break
        }
    }
    
    func configureLock() {
        clearView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        clearView.backgroundColor = UIColor.clear
        unlockSlider = AURUnlockSlider(frame: CGRect(x: 0, y: self.view.frame.height - 100, width: self.view.bounds.size.width, height: 70.0))
        unlockSlider.sliderBackgroundColor = UIColor.black
        unlockSlider.delegate = self
        addLock()
    }
    
    func addLock() {
        self.view.addSubview(clearView)
        clearView.addSubview(unlockSlider)
    }
    
    func unlockSliderDidUnlock(_ snapSwitch: AURUnlockSlider) {
        unlockSlider = nil
        clearView.removeFromSuperview()
        clearView = nil
    }
}
