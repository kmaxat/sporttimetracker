//
//  Routes.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 22.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper

class Routes: Mappable {

    
    var meta: Meta?

    var data: [dataRoute]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        meta <- map["meta"]
        data <- map["data"]
    }
}

class dataRoute: Mappable {

    var id: Int = 0

    var title = String()

    var picture: String?

    var private_state = String()

    var distance: Double = 0

    var route = [LocationObject]()
    
    var records = [Records]()
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        picture <- map["picture"]
        private_state <- map["private_state"]
        route <- map["route"]
        distance <- map["distance"]
        records <- map["records"]
    }

}

class Place: Mappable {
    
    var place: Int = 0
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        place <- map["place"]
    }
}

class RecordsArr: Mappable {
    
    var meta: Meta?
    
    var data: [Records]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        meta <- map["meta"]
        data <- map["data"]
    }
}


class Records: Mappable {

    var id: Int = 0
    
    var sport = dataSport()

    var user = User()

    var duration_seconds: Int = 0
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        user <- map["user"]
        sport <- map["sport"]
        duration_seconds <- map["duration_seconds"]
    }


}

