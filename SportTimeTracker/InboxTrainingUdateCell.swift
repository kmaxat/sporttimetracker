//
//  InboxTrainingUdateCell.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 27.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

class InboxTrainingUdateCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var txtText: UILabel!
    
    var training: DataGT!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
