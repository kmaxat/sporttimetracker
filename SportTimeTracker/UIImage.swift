//
//  ExtensionUIImage.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 17/10/2016.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

extension UIImage {
    
    private func _resizeWithAspect_doResize( _ image: UIImage,size: CGSize) -> UIImage {
        if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)){
            UIGraphicsBeginImageContextWithOptions(size,false,UIScreen.main.scale);
        }
        else
        {
            UIGraphicsBeginImageContext(size);
        }
        
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height));
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return newImage!;
    }
    
    func resizeImageWithAspect(scaledToMaxWidth width:CGFloat,maxHeight height :CGFloat)->UIImage {
        let oldWidth = self.size.width;
        let oldHeight = self.size.height;
        
        let scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
        
        let newHeight = oldHeight * scaleFactor;
        let newWidth = oldWidth * scaleFactor;
        let newSize = CGSize(width: newWidth, height: newHeight);
        
        return _resizeWithAspect_doResize(self, size: newSize);
    }
}
