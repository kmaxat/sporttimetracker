//
//  ResultsTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 09.07.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit

class ResultsTC: UITableViewController {
    
    var data = [Records]()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                                               for: indexPath) as! ResultsCell
        let record = self.data[(indexPath as NSIndexPath).row]
        cell.txtPlace.text = "\((indexPath as NSIndexPath).row + 1)"
        cell.txtName.text = record.user.first_name + " " + record.user.last_name
        cell.txtRecord.text = stringFromTimeInterval(sec: record.duration_seconds)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profileVC = st.instantiateViewController(withIdentifier: "User") as! UserProfileVC
        profileVC.userID = self.data[(indexPath as NSIndexPath).row].user.id
        profileVC.hideCloseBtn = true
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

}
