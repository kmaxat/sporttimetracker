//
//  ChoosePlaceVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 16.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import MapKit

protocol ChoosePlaceDelegate {
    func didiChoosePlace(_ loc: LocationObject)
}

class ChoosePlaceVC: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet fileprivate weak var map: MKMapView?
    var locationManager = CLLocationManager()
    
    var viewModel: CreateTrainingViewModel!
    
    var delegate: ChoosePlaceDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        let gestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(ChoosePlaceVC.handleTap(_:)))
        gestureRecognizer.delegate = self
        map!.addGestureRecognizer(gestureRecognizer)
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        if self.viewModel.place != nil {
            var mapRegion = MKCoordinateRegion()
            let coordinate = CLLocationCoordinate2D(latitude: viewModel.place!.lat, longitude: viewModel.place!.lon)
            mapRegion.center = coordinate
            mapRegion.span.latitudeDelta = 0.06
            mapRegion.span.longitudeDelta = 0.06
            
            self.map!.setRegion(mapRegion, animated: true)
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            
            self.map!.addAnnotation(annotation)
        }
    }
    
    func handleTap(_ gestureReconizer: UILongPressGestureRecognizer) {
        
        if map!.annotations.count > 0 {
            map!.removeAnnotation(map!.annotations[0])
        }
        
        let location = gestureReconizer.location(in: map!)
        let coordinate = map!.convert(location,toCoordinateFrom: map!)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        map!.addAnnotation(annotation)
        
        let myLocation = LocationObject()
        myLocation.lat = coordinate.latitude
        myLocation.lon = coordinate.longitude
        
        delegate?.didiChoosePlace(myLocation)
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        var mapRegion = MKCoordinateRegion()
        mapRegion.center = map!.userLocation.coordinate
        mapRegion.span.latitudeDelta = 0.05
        mapRegion.span.longitudeDelta = 0.05
        
        locationManager.stopUpdatingLocation()
        mapView.showsUserLocation = false
        map!.setRegion(mapRegion, animated: true)
    }
}
