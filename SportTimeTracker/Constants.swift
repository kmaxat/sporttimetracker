//
//  Constants.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 09.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import KeychainSwift
import MapKit
import Moya

class Constants {
    struct colors {
        static let appMainColor: UIColor = UIColor(hex: "0c0f24")
    }
}

let avatarImage = UIImage(named: "avatar_placeholder")
let keychain = KeychainSwift()
let st = UIStoryboard(name: "Main", bundle: nil)
func showAlert(_ title: String, message: String, controllder: UIViewController) {
    let alertController = UIAlertController(title: title, message:
        message, preferredStyle: UIAlertControllerStyle.alert)
    alertController.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.default,handler: nil))
    
    controllder.present(alertController, animated: true, completion: nil)
}

//MARK - strings

func stringFromTimeInterval(sec: Int) -> String {
    let seconds = sec % 60
    let minutes = (sec/60) % 60
    let hours = (sec / 3600) % 24
    return String(format: "%0.2d:%0.2d:%0.2d", arguments: [hours, minutes, seconds])
}

func stringPaceFromTimeInterval(sec: Int) -> String {
    
    let unit =  keychain.get("unit")
    var currentUnit = ""
    switch unit! {
    case "mi":
        currentUnit = " мин/миль"
    default:
        currentUnit = " мин/км"
    }
    
    let seconds = sec % 60
    let minutes = (sec/60) % 60
    let hours = (sec / 3600) % 24
    var pace = ""
    if(hours > 0) {
        pace =  String(format: "%0.2d:%0.2d:%0.2d", arguments: [hours, minutes, seconds])
    } else {
        pace =  String(format: "%0.2d:%0.2d", arguments: [minutes, seconds])
    }
    return pace + currentUnit
}

func stringSpeed(_ double: Double) -> String {
    let unit =  keychain.get("unit")
    var currentUnit = ""
    switch unit! {
    case "mi":
        currentUnit = " миля/ч"
    default:
        currentUnit = " км/ч"
    }
    let speed = String(format: "%.2f", double)
    return speed + currentUnit
}

func stringCalories(_ double: Double) -> String {
    let calories = String(format: "%.2f", double)
    return calories + " ккал"
}


func stringKM(_ distanceMeters: Double) -> String {
    let unit =  keychain.get("unit")
    var currentUnit = ""
    switch unit! {
    case "mi":
        currentUnit = "миля"
    default:
        currentUnit = "км"
    }
    let distance = String(format: "%.2f %@", distanceMeters, "")

    if(distanceMeters < 1.0){
        return  distance + "м"
    }
    else {
        return distance + currentUnit
    }
}

func to_send_stringFormatDateToServer(_ date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-d"
    return dateFormatter.string(from: date)
}

func to_send_stringFormatDateToServer_full(_ date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return dateFormatter.string(from: date)
}

func dateFromString(_ str: String) -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return dateFormatter.date(from: str)!
}

func to_show_stringFromDateWithTime(_ start_date: String, start_time: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-d HH:mm:ss"
    let beatyFormatter = DateFormatter()
    beatyFormatter.dateFormat = "EEEE, dd MMMM yyyy HH:mm"
    let dateStr = start_date + " " + start_time
    let date = dateFormatter.date(from: dateStr)
    return beatyFormatter.string(from: date!)
}

func to_show_stringFromDate(_ date: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-d HH:mm:ss"
    let beatyFormatter = DateFormatter()
    beatyFormatter.dateFormat = "EEEE, dd MMMM yyyy HH:mm"
    let date = dateFormatter.date(from: date)
    return beatyFormatter.string(from: date!)
}

func to_show_stringFromDate_no_time(_ date: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-d"
    let beatyFormatter = DateFormatter()
    beatyFormatter.dateFormat = "dd MMMM yyyy"
    if let date = dateFormatter.date(from: date) {
        return beatyFormatter.string(from: date)
    } else {
        return ""
    }
}

func dateFromStartDateAndTime(_ start_date: String, start_time: String) -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-d HH:mm:ss"
    return dateFormatter.date(from: start_date + " " + start_time)!
}


func showLocalPush(_ text: String) {
    let notification = UILocalNotification()
    notification.fireDate = Date(timeIntervalSinceNow: 5)
    notification.alertBody = text
    notification.alertAction = ""
    notification.soundName = UILocalNotificationDefaultSoundName
    notification.userInfo = ["CustomField1": "w00t"]
    UIApplication.shared.scheduleLocalNotification(notification)
}

func showErrorAlertWithCode(code: Any, controller: UIViewController) {
    let alertController = UIAlertController(title: "Error", message:
        "\(code)", preferredStyle: UIAlertControllerStyle.alert)
    alertController.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.default,handler: nil))
    controller.present(alertController, animated: true, completion: nil)
}
