//
//  TrainingDetailTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 17.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Nuke
import MapKit
import IBAnimatable
import PKHUD
import FBSDKCoreKit

class TrainingDetailVC: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var btnGo: UIButton!
    @IBOutlet weak var btnMb: UIButton!
    @IBOutlet weak var btnInvited: UIButton!
    @IBOutlet weak var txtDescr: UILabel!
    @IBOutlet weak var btnViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imgSport: UIImageView!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var txtSport: UILabel!
    @IBOutlet weak var txtDate: UILabel!
    @IBOutlet weak var txtOrg: UILabel!
    @IBOutlet weak var orgAvatar: UIImageView!
    @IBOutlet weak var countGo: UILabel!
    @IBOutlet weak var countMayBe: UILabel!
    @IBOutlet weak var countInvited: UILabel!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var geBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var btnView: AnimatableView!
    var editBtn: UIButton!
    var trainingID: Int?
    var training: DataGT?
    let sharer = Sharer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let usersGoVC = st.instantiateViewController(withIdentifier: "userCounts") as! UsersCountsTC
        
        sharer.controller = self
        
        fbBtn.rx.tap.bindNext {
            self.sharer.shareTrainingToFB(tr: self.training!)
        }.addDisposableTo(rx_disposeBag)
        
        geBtn.rx.tap.bindNext {
            self.sharer.shareTrainingToGE(training: self.training!)
        }.addDisposableTo(rx_disposeBag)
        
        shareBtn.rx.tap.bindNext {
            self.sharer.shareTraining(training: self.training!)
        }.addDisposableTo(rx_disposeBag)
        
        btnGo.rx.tap.bindNext {
            usersGoVC.trainingID = self.trainingID!
            usersGoVC.type = "accepted"
            self.navigationController?.pushViewController(usersGoVC, animated: true)
        }.addDisposableTo(rx_disposeBag)
        
        btnMb.rx.tap.bindNext {
            usersGoVC.trainingID = self.trainingID!
            usersGoVC.type = "maybe"
            self.navigationController?.pushViewController(usersGoVC, animated: true)
        }.addDisposableTo(rx_disposeBag)
        
        btnInvited.rx.tap.bindNext {
            usersGoVC.trainingID = self.trainingID!
            usersGoVC.type = "invited"
            self.navigationController?.pushViewController(usersGoVC, animated: true)
        }.addDisposableTo(rx_disposeBag)
        
        let editImage = UIImage(named: "edit")
        self.editBtn = UIButton(type: .custom)
        self.editBtn.bounds = CGRect(x: 0, y: 0, width: 30, height: 30)
        self.editBtn.setImage(editImage, for: UIControlState())
        let editButton   = UIBarButtonItem(customView: self.editBtn)
        self.navigationItem.rightBarButtonItems = [editButton]
        
        editBtn.rx.tap.bindNext {
            let trainingCreateVC = st.instantiateViewController(withIdentifier: "trainingCreate") as! TrainingCreateTC
            trainingCreateVC.training = self.training!
            self.navigationController?.pushViewController(trainingCreateVC, animated: true)
        }.addDisposableTo(rx_disposeBag)
        
        saveBtn?.rx.tap.bindNext({
            HUD.show(.progress)
            Network.request(target: .groupTrainingDecision(id: self.trainingID!, decision: "accepted"))
            .subscribe(onNext: { (response) in
                if response.statusCode < 300 {
                    showAlert("SportTime", message: "Вы учавствуете в тренировке", controllder: self)
                } else {
                    showErrorAlertWithCode(code: response.statusCode, controller: self)
                }
                }, onError: { (error) in
                    HUD.hide()
                    showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                }, onCompleted: { 
                    HUD.hide()
                }, onDisposed: nil).addDisposableTo(self.rx_disposeBag)
        }).addDisposableTo(rx_disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if trainingID != nil {
            HUD.show(.progress)
            Network.request(target: .getGroupTrainingWithIID(id: trainingID!))
                .mapObject(DataGT.self)
                .subscribe(onNext: { (response) in
                    self.training = response
                    self.title = self.training?.title
                    let userID = Int(keychain.get("id")!)
                    if userID == response.user.id {
                        self.saveBtn?.isHidden = true
                        self.btnViewHeight.constant = 0
                        self.editBtn.isHidden = false
                    } else {
                        self.editBtn.isHidden = true
                    }
                    
                    if let url = URL(string: response.user.picture ?? "") {
                        Nuke.loadImage(with: url, into: self.orgAvatar)
                    }else {
                        self.orgAvatar.image = #imageLiteral(resourceName: "avatar_placeholder")
                    }
                    
                    Nuke.loadImage(with: URL(string: response.sport.picture)!, into: self.imgSport)
                    self.txtSport.text = response.sport.title
                    self.txtDate.text = to_show_stringFromDateWithTime(response.start_date, start_time: response.start_time)
                    self.txtOrg.text = response.user.first_name + " " + response.user.last_name
                    self.countGo.text = "\(response.count_accepted)"
                    self.countMayBe.text = "\(response.count_maybe)"
                    self.countInvited.text = "\(response.count_invited)"
                    self.txtDescr.text = response.desc
                    
                    if self.map!.annotations.count > 0 {
                        self.map!.removeAnnotation(self.map!.annotations[0])
                    }
                    
                    var mapRegion = MKCoordinateRegion()
                    let coordinate = CLLocationCoordinate2D(latitude: response.location.lat, longitude: response.location.lon)
                    mapRegion.center = coordinate
                    mapRegion.span.latitudeDelta = 0.06
                    mapRegion.span.longitudeDelta = 0.06
                    
                    self.map!.setRegion(mapRegion, animated: true)
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = coordinate
                    self.map!.addAnnotation(annotation)
                    self.map?.isScrollEnabled = false
                    }, onError: { (error) in
                        HUD.hide()
                        showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                    }, onCompleted: {
                        HUD.hide()
                    }, onDisposed: nil).addDisposableTo(rx_disposeBag)
        }
    }
}