//
//  DistanceCalculator.h
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 28.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface DistanceCalculator : NSObject

- (BOOL)isCoordinate:(CLLocationCoordinate2D)coordinate closeToPolyline:(MKPolyline *)polyline;

@end
