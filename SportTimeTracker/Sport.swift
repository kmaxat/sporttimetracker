//
//  Sport.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Sport: Mappable {
    
    var data: [dataSport]?

    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
    
}

class dataSport: Object, Mappable {
    
    dynamic var id: Int = 0
    
    dynamic var title = String()
    
    dynamic var picture = String()
    
    dynamic var title_eng = String()
    
    dynamic var calories: Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        picture <- map["picture"]
        title_eng <- map["title_eng"]
        calories <- map["calories"]
    }
}

