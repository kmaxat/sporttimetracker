//
//  MainWindowController.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 10/10/2016.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Pages

class MainWindowController {
    
    class func setLeft() {
        let mainViewController = st.instantiateViewController(withIdentifier: "training")
        let leftViewController = st.instantiateViewController(withIdentifier: "left") as! LeftMenu
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
        makeRootWindow(vc: slideMenuController)
    }
    
    class func setPages() {
        let storyboardIds = ["One","Two", "Three"]
        let pages = PagesController(storyboardIds)
        MainWindowController.makeRootWindow(vc: pages)
    }
    
    class func makeRootWindow(vc: UIViewController) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = vc
        appDelegate.window?.makeKeyAndVisible()
    }
}
