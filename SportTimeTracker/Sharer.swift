//
//  Sharer.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 22.09.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import SafariServices

class Sharer: NSObject {

    var controller: UIViewController?
    
    func shareRouteFB(training: DataTR) {
        if FBSDKAccessToken.current() != nil {
            var properties: [String : AnyObject] = [
                "og:type": "fitness.course" as AnyObject,
                "og:title": training.sport.title as AnyObject,
                "og:description": stringToShareRoute(training: training) as AnyObject,
                "fitness:duration:value": training.duration/10 as AnyObject,
                "fitness:duration:units": "s" as AnyObject,
                "fitness:distance:value": training.distance/1000 as AnyObject,
                "fitness:distance:units": "km" as AnyObject,
                "fitness:speed:value": training.max_speed as AnyObject,
                "fitness:speed:units": "m/s" as AnyObject
            ]
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm'"
            var i = 0
            for point in training.route {
                properties["fitness:metrics[\(i)]:location:latitude"] = point.lat as AnyObject?
                properties["fitness:metrics[\(i)]:location:longitude"] = point.lon as AnyObject?
                properties["fitness:metrics[\(i)]:location:altitude"] = 0 as AnyObject?
                properties["fitness:metrics[\(i)]:timestamp"] = formatter.string(from: Date()) as AnyObject?
                i += 1
                print(i)
            }
            let object = FBSDKShareOpenGraphObject(properties: properties);
            let action = FBSDKShareOpenGraphAction()
            action.actionType = "fitness.Run"
            action.setObject(object, forKey: "fitness:course")
            action.setString("true", forKey: "fb:explicitly_shared")
            let content = FBSDKShareOpenGraphContent()
            content.action = action
            content.previewPropertyName = "fitness:course"
            FBSDKShareDialog.show(from: controller, with: content, delegate: self)
        } else {
            let login = FBSDKLoginManager()
            login.logIn(withPublishPermissions: ["publish_actions"], from: controller, handler: { (result, error) in
                if result != nil {
                    keychain.set(result!.token.tokenString, forKey: "fbToken")
                    showAlert("SportTime", message: "Авторизация получена, нажмите еще раз", controllder: self.controller!)
                }
            })
        }
    }
    
    func shareRouteToGE(training: DataTR) {
        let text = stringToShareRoute(training: training)
        shareGE(text: text)
    }
    
    func shareRoute(training: DataTR) {
        let objectsToShare = [stringToShareRoute(training: training)]
        share(shareObjects: objectsToShare)
    }
    
    func shareTrainingToFB (tr: DataGT) {
        if FBSDKAccessToken.current() != nil {
            let properties: [String : AnyObject] = [
                "og:url": "http://sporttimeapp.com/trainings/?training_id=\(tr.id)" as AnyObject,
                "og:type": "sportime_club:training" as AnyObject,
                "og:title": tr.title as AnyObject,
                "sportime_club:sport": tr.sport.title as AnyObject,
                "og:description": tr.desc as AnyObject? ?? "" as AnyObject,
                "place:location:latitude": tr.location.lat as AnyObject,
                "place:location:longitude": tr.location.lon as AnyObject,
                "place:location:altitude": 0 as AnyObject
            ]

            let object = FBSDKShareOpenGraphObject(properties: properties)
            let action = FBSDKShareOpenGraphAction()
            action.actionType = "sportime_club:create"
            action.setObject(object, forKey: "training")
            action.setString("true", forKey: "fb:explicitly_shared")
            let content = FBSDKShareOpenGraphContent()
            content.action = action
            content.previewPropertyName = "training"
            FBSDKShareDialog.show(from: controller, with: content, delegate: self)
        } else {
            let login = FBSDKLoginManager()
            login.logIn(withPublishPermissions: ["publish_actions"], from: controller, handler: { (result, error) in
                if result != nil {
                    keychain.set(result!.token.tokenString, forKey: "fbToken")
                    showAlert("SportTime", message: "Авторизация получена, нажмите еще раз", controllder: self.controller!)
                }
            })
        }
    }
    
    func shareTrainingToGE(training: DataGT) {
        let text = stringToShareTraining(tr: training)
        shareGE(text: text)
    }
    
    func shareTraining(training: DataGT) {
        let objectsToShare = [stringToShareTraining(tr: training)]
        share(shareObjects: objectsToShare)
    }
    
    fileprivate func shareGE(text: String) {
        let urlComponents = NSURLComponents(string: "https://plus.google.com/share")
        let queryItem = NSURLQueryItem(name: "text", value: text)
        urlComponents?.queryItems = [queryItem as URLQueryItem]
        let url = urlComponents?.url
        let safariVC = SFSafariViewController(url: url!)
        controller?.present(safariVC, animated: true, completion: nil)
    }
    
    fileprivate func share(shareObjects: [Any]) {
        let activityVC = UIActivityViewController(activityItems: shareObjects, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = controller?.view
        controller!.present(activityVC, animated: true, completion: nil)
    }
}

extension Sharer: FBSDKSharingDelegate {
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print(error.localizedDescription)
    }
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
}

extension Sharer {
    func stringToShareRoute(training: DataTR) -> String {
        var startStr = "Завершил тренировку"
        if training.sport.id == 21 {
            startStr = "Пробежал \(stringKM(training.distance))"
        } else if training.sport.id == 7 || training.sport.id == 8 || training.sport.id == 19 {
            startStr = "Проехал \(stringKM(training.distance))"
        }
        return startStr + " за \(stringFromTimeInterval(sec: training.duration)) и сжег \(training.calories) кал."
    }
    
    func stringToShareTraining(tr: DataGT) -> String {
        return "\(tr.user.first_name) \(tr.user.last_name) создал групповую тренировку \(tr.title) \(to_show_stringFromDateWithTime(tr.start_date, start_time: tr.start_time))"
    }
}
