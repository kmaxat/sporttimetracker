//
//  LocationService.swift
//  ZJBatterySaveLocation
//
//  Created by ZeroJianMBP on 16/4/19.
//  Copyright © 2016年 ZeroJian. All rights reserved.
//

import CoreLocation
import UIKit

protocol LocationDelegate {
    func didUpdateLocation(_ newLocation: CLLocation)
}

class ZJLocationService: NSObject, CLLocationManagerDelegate {
  
  static let sharedManager = ZJLocationService()
    
    var delegate: LocationDelegate?

  fileprivate var backgroundTask = BackgroundTask()
  fileprivate var timeInterval: Double = 179
  fileprivate var timer: Timer?
  
  class var time: TimeInterval! {
    get {
      return self.sharedManager.timeInterval
    }
    set(second) {
      if second > 0 && second < 180 {
        self.sharedManager.timeInterval = second
      }
    }
  }
  
  fileprivate var backgroundLocations = [CLLocation]() {
    didSet{
      if backgroundLocations.count == 10 {
        begionBackgroundTask(self.timeInterval)
      }
    }
  }
  
  var didUpdateLocation: ((CLLocation) -> Void)?
  
  class func startLocation() {
    if (CLLocationManager.locationServicesEnabled()) {
      self.sharedManager.locationManager.startUpdatingLocation()
      print("begin updating location")
    }
  }
  
  class func stopLocation() {
    self.sharedManager.locationManager.stopUpdatingLocation()
    print("did stop location")
  }
  
  lazy var locationManager: CLLocationManager = {
    let locationManager = CLLocationManager()
    locationManager.delegate = self
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    locationManager.pausesLocationUpdatesAutomatically = false
    locationManager.distanceFilter = 2
    locationManager.activityType = .fitness
    if #available(iOS 9.0, *) {
      locationManager.allowsBackgroundLocationUpdates = true
    }
    locationManager.requestAlwaysAuthorization()
    return locationManager
  }()
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let newLocation = locations.last else { return }
    
    didUpdateLocation?(newLocation)
    delegate?.didUpdateLocation(newLocation)
    
    if UIApplication.shared.applicationState != .active {
      print("background location : \(newLocation.coordinate.latitude), \(newLocation.coordinate.longitude)")
      backgroundLocations.append(newLocation)
    } else {
      print("active status location:  \(newLocation.coordinate.latitude), \(newLocation.coordinate.longitude)")
      
      initialBackgroundTask()
      
      if backgroundLocations.count > 0 {
        backgroundLocations.removeAll()
      }
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    switch status {
    case .denied:
      showAlert()
      print("AuthorizationStatus -- Denied")
    default:
      break
    }
  }
  
  fileprivate func showAlert() {
      let alertController = UIAlertController(title: "Confirm", message: "This App does not have access to Location Services,Please enable in Settings", preferredStyle: .alert)
      let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
      alertController.addAction(action)
      
     currentViewController()?.present(alertController, animated: true, completion: nil)
  }
  
  func currentViewController() -> UIViewController? {
    guard let rootViewController = UIApplication.shared.keyWindow?.rootViewController else {
      return nil
    }
    if let presentedViewController = rootViewController.presentedViewController{
      return presentedViewController
    }else{
      return rootViewController
    }
  }
  
  var lastBackgroundLocation: ((CLLocation) -> Void)?
  
  fileprivate func begionBackgroundTask(_ time: TimeInterval){
    initialBackgroundTask()
    
    lastBackgroundLocation?(backgroundLocations.last!)
    backgroundLocations.removeAll()
    
    timer = Timer.scheduledTimer(timeInterval: time, target: self, selector: #selector(againStartLocation), userInfo: nil, repeats: false)
    
    backgroundTask.registerBackgroundTask()
    
    print(" stop location :\(time) seconds")
  }
  
  fileprivate func initialBackgroundTask() {
    if backgroundTask.tasking {
      backgroundTask.endBackgroundTask()
    }
    if let timer = timer {
      timer.invalidate()
    }
  }
  
  func againStartLocation(){
    
    ZJLocationService.startLocation()
  }
}
