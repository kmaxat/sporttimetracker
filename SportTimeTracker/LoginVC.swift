//
//  LoginVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 09.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import PKHUD
import FBSDKLoginKit
import Simplicity

class LoginVC: UIViewController, UITextFieldDelegate {

    @IBOutlet fileprivate weak var closeBtn: UIButton?
    @IBOutlet fileprivate weak var enterBtn: UIButton?
    @IBOutlet fileprivate weak var txtEmail: UITextField?
    @IBOutlet fileprivate weak var txtPassword: UITextField?
    @IBOutlet fileprivate weak var googleBtn: UIButton?
    @IBOutlet fileprivate weak var facebookBtn: UIButton?
    var email: String?
    var password: String?
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        closeBtn?.rx.tap.bindNext({
            self.dismiss(animated: true, completion: nil)
        }).addDisposableTo(bag)
        
        facebookBtn?.rx.tap.bindNext({
            let login = FBSDKLoginManager()
            login.logIn(withReadPermissions: ["email"], from: self, handler: { (result, error) in
                HUD.show(.progress)
                if result?.token != nil {
                    keychain.set((result?.token.tokenString)!, forKey: "fbToken")
                    Network.request(target: .loginFB(token: (result?.token.tokenString)!))
                    .mapObject(Token.self)
                    .subscribe(onNext: { (token) in
                        self.gotToken(token: token)
                        }, onError: { (error) in
                            HUD.hide()
                            showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                        }, onCompleted: { 
                            HUD.hide()
                        }, onDisposed: nil).addDisposableTo(self.bag)
                } else {
                    HUD.hide()
                }
            })
        }).addDisposableTo(bag)
        
        googleBtn?.rx.tap.bindNext({
            let google = Google()
            google.scopes = ["email"]
            Simplicity.login(google, callback: { (accessToken, error) in
                keychain.set(accessToken!, forKey: "geToken")
                Network.request(target: .loginGE(token: accessToken!))
                    .mapObject(Token.self)
                    .subscribe(onNext: { (token) in
                        self.gotToken(token: token)
                        }, onError: { (error) in
                            HUD.hide()
                            showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                        }, onCompleted: {
                            HUD.hide()
                        }, onDisposed: nil).addDisposableTo(self.bag)
            })
        }).addDisposableTo(bag)
        
        txtEmail?.rx.textInput.text.bindNext({ (str) in
            self.email = str
        }).addDisposableTo(bag)
        
        txtPassword?.rx.textInput.text.bindNext({ (str) in
            self.password = str
        }).addDisposableTo(bag)
        
        enterBtn?.rx.tap.bindNext({
            if self.email != nil && self.password != nil {
                if self.validateCredentials(self.email!, password: self.password!) == true {
                    HUD.show(.progress)
                    keychain.set(self.email!, forKey: "email")
                    keychain.set(self.password!, forKey: "password")
                    Network.request(target: .loginUser(email: self.email!, password: self.password!))
                        .mapObject(Token.self)
                        .subscribe(onNext: { (token) in
                            self.gotToken(token: token)
                            }, onError: { (error) in
                                HUD.hide()
                                showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                            }, onCompleted: { 
                                HUD.hide()
                            }, onDisposed: nil).addDisposableTo(self.bag)
                }
            }
        }).addDisposableTo(bag)
    }
    
    func gotToken(token: Token) {
        if token.token != nil {
            keychain.set(token.token!, forKey: "token_sp")
            self.getUser()
        } else {
            HUD.hide()
            showAlert("SportTime", message: "Error", controllder: self)
        }
    }

    func getUser() {
        Network.request(target: .getUserMe())
            .mapObject(User.self)
            .subscribe(onNext: { (user) in
                keychain.set("\(user.id)", forKey: "id", withAccess: .accessibleAfterFirstUnlock)
                keychain.set(user.first_name, forKey: "first_name", withAccess: .accessibleAfterFirstUnlock)
                keychain.set(user.last_name, forKey: "last_name", withAccess: .accessibleAfterFirstUnlock)
                keychain.set(user.email, forKey: "email", withAccess: .accessibleAfterFirstUnlock)
                keychain.set(user.gender, forKey: "gender", withAccess: .accessibleAfterFirstUnlock)
                keychain.set(user.unit.rawValue, forKey: "unit", withAccess: .accessibleAfterFirstUnlock)
                self.login()
            }).addDisposableTo(self.bag)
    }

    func login() {
        HUD.hide()
        MainWindowController.setLeft()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    fileprivate func validateCredentials(_ email: String, password: String) -> Bool {
        let emailRule = email.characters.count > 5
        let passwordRule = password.characters.count >= 6
        return emailRule && passwordRule
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.txtEmail!:
            self.txtPassword?.becomeFirstResponder()
            break
        case self.txtPassword!:
            self.txtPassword?.resignFirstResponder()
            break
        default:
            break
        }
        return true
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.view.frame.origin.y < 0 {
            animateViewMoving(false, moveValue: 100)
        }
    }
    
    func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
}
