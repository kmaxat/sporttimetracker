//
//  Inbox.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper

enum inboxType: String, RawRepresentable {
    case group_training_update// = "group_training_update"
    case group_training_invitation// = "group_training_invitation"
    case friend_request// = "friend_request"
}

enum updateType: String, RawRepresentable {
    case changed_location// = "changed_location"
    case changed_sport// = "changed_sport"
    case changed_datetime// = "changed_datetime"
    case changed_else// = "changed_else"
}

class Inbox: Mappable {
    
    var data: [inboxData]?
    

    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
    
}

class inboxData: Mappable {
    
    var id = Int()

    var type: inboxType!
    
    var updated_field: updateType!

    var user: User?
    
    var group_training: DataGT?
    
    var date = String()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        updated_field <- map["updated_field"]
        user <- map["user"]
        group_training <- map["group_training"]
        date <- map["date"]
    }
}

