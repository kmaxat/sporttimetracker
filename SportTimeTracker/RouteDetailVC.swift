//
//  RouteDetailTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 17.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import IBAnimatable
import PKHUD
import Simplicity
import Nuke

class RouteDetailVC: UIViewController, NSMachPortDelegate {

    @IBOutlet fileprivate weak var map: MKMapView!
    @IBOutlet weak var imgSport: UIImageView!
    @IBOutlet fileprivate weak var txtSport: UILabel!
    @IBOutlet fileprivate weak var txtDate: UILabel!
    @IBOutlet fileprivate weak var txtDistance: UILabel!
    @IBOutlet fileprivate weak var txtSpeed: UILabel!
    @IBOutlet fileprivate weak var txtPace: UILabel!
    @IBOutlet fileprivate weak var txtCalories: UILabel!
    @IBOutlet fileprivate weak var txtTime: UILabel!
    @IBOutlet fileprivate weak var txtAvgSpeed: UILabel!
    @IBOutlet fileprivate weak var txtAvgPace: UILabel!
    @IBOutlet fileprivate weak var fbBtn: UIButton!
    @IBOutlet fileprivate weak var geBtn: UIButton!
    @IBOutlet fileprivate weak var shareBtn: UIButton!
    @IBOutlet fileprivate weak var saveBtn: UIButton!
    @IBOutlet weak var btnView: AnimatableView!
    var routeID: Int?
    var training: DataTR?
    var isFollowRoute = Bool()
    var hideBtn = Bool()
    let sharer = Sharer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        binds()
        self.saveBtn?.isHidden = hideBtn
        self.btnView.isHidden = hideBtn
        
        sharer.controller = self
        
        if routeID != nil && isFollowRoute == false {
            getTraining()
            self.saveBtn?.setTitle("Следовать маршруту", for: UIControlState())
        } else if training != nil && isFollowRoute == false {
            fillView(training!)
            bindSave()
        } else if isFollowRoute == true {
            fillView(training!)
            showPlace()
            bindResults()
        }
    }
    
    func binds() {
        fbBtn.rx.tap.bindNext({
            self.shareToFb()
        }).addDisposableTo(rx_disposeBag)
        
        geBtn.rx.tap.bindNext {
            self.shareToGe()
        }.addDisposableTo(rx_disposeBag)
        
        shareBtn.rx.tap.bindNext {
            self.share()
        }.addDisposableTo(rx_disposeBag)
    }
    
    func showPlace() {
        Network.request(target: .postRouteTime(id: self.routeID!, sport_id: training!.sport.id, duration_seconds: training!.duration))
            .mapObject(Place.self)
            .bindNext({ (place) in
                showAlert("SportTime", message: "Вы заняли \(place.place) место", controllder: self)
            }).addDisposableTo(rx_disposeBag)
    }
    
    func bindSave() {
        self.saveBtn.setTitle("Сохранить маршрут", for: UIControlState())
        self.saveBtn.rx.tap
            .bindNext({
                let saveVC = st.instantiateViewController(withIdentifier: "saveTraining") as! SaveTrainingVC
                saveVC.training = self.training
                self.navigationController?.pushViewController(saveVC, animated: true)
            }).addDisposableTo(rx_disposeBag)
    }
    
    func bindResults() {
        self.saveBtn.setTitle("Сравнить с другими", for: UIControlState())
        self.saveBtn.rx.tap
            .bindNext({
                Network.request(target: .getAllRecordsForRoute(id: self.routeID!))
                    .mapObject(RecordsArr.self)
                    .subscribe(onNext: { (records) in
                        let recordsVC = st.instantiateViewController(withIdentifier: "records") as! ResultsTC
                        recordsVC.data = records.data ?? []
                        self.navigationController?.pushViewController(recordsVC, animated: true)
                        }, onError: { (error) in
                            HUD.hide()
                            showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                        }, onCompleted: {
                            HUD.hide()
                        }, onDisposed: nil).addDisposableTo(self.rx_disposeBag)
            }).addDisposableTo(rx_disposeBag)
    }
    
    func getTraining() {
        HUD.show(.progress)
        Network.request(target: .getTrainingWithID(id: self.routeID!))
            .mapObject(DataTR.self)
            .subscribe(onNext: { (training) in
                self.fillViewFromServer(training: training)
                }, onError: { (error) in
                    HUD.hide()
                    showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                }, onCompleted: {
                    HUD.hide()
                }, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    
    func fillViewFromServer(training: DataTR) {
        let calculator = IndicatorsCalculator()
        Nuke.loadImage(with: URL(string: training.sport.picture)!, into: self.imgSport)
        txtSport.text = training.sport.title
        txtDate.text = to_show_stringFromDate(training.date)
        txtDistance.text = stringKM(calculator._distance(inputDistance: training.distance))
        txtSpeed.text = stringSpeed(training.max_speed)
        txtPace.text = stringPaceFromTimeInterval(sec: Int(training.max_pace))
        txtTime.text = stringFromTimeInterval(sec: training.duration)
        txtAvgSpeed.text = stringSpeed(training.average_speed)
        txtAvgPace.text = stringPaceFromTimeInterval(sec: Int(training.average_pace))
        txtCalories.text = stringCalories(training.calories)
        self.training = training
        
        let locations = training.route.map{
            return CLLocationCoordinate2D(latitude: $0.lat, longitude: $0.lon)
        }
        
        map.addPolygon(points: locations.count > 0 ? locations : training.route2d)
        map.isScrollEnabled = false
    }
    
    func fillView(_ training: DataTR) {
        self.training = training
        let calculator = IndicatorsCalculator()
        calculator.locations = training.locations//.route2d.map{CLLocation(latitude: $0.latitude, longitude: $0.longitude)}
        calculator.distance = training.distance
        calculator.speed = training.max_speed
        calculator.seconds = training.duration
        calculator.calculateIndicators()
        
        Nuke.loadImage(with: URL(string: training.sport.picture)!, into: self.imgSport)
        txtSport.text = training.sport.title
        txtDate.text = to_show_stringFromDate(training.date)
        txtDistance.text = stringKM(calculator._distance())
        txtSpeed.text = stringSpeed(calculator._speed())
        txtPace.text = stringPaceFromTimeInterval(sec: Int(calculator._pace()))
        txtTime.text = stringFromTimeInterval(sec: training.duration)
        txtAvgSpeed.text = stringSpeed(calculator._avgSpeed())
        txtAvgPace.text = stringPaceFromTimeInterval(sec: Int(calculator._avgPace()))
        txtCalories.text = stringCalories(training.calories)
        
        let locations = training.route.map{
            return CLLocationCoordinate2D(latitude: $0.lat, longitude: $0.lon)
        }
        
        map.addPolygon(points: locations.count > 0 ? locations : training.route2d)
        map.isScrollEnabled = false
    }
    
    func mapView(_ mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        return self.map.lineView(rendererForOverlay: overlay)
    }
}

extension RouteDetailVC {
    func shareToFb() {
        sharer.shareRouteFB(training: self.training!)
    }
    
    func shareToGe() {
        sharer.shareRouteToGE(training: self.training!)
    }
    
    func share() {
        sharer.shareRoute(training: self.training!)
    }
}
