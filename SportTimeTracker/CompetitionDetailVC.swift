//
//  CompetitionDetailVC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 22.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import PKHUD

class CompetitionDetailVC: UIViewController {

    @IBOutlet weak var recordsBtn: UIButton!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var txtDistance: UILabel!
    @IBOutlet weak var txtSport: UILabel!
    @IBOutlet weak var txtTime: UILabel!
    @IBOutlet weak var followBtn: UIButton!
    var competition: dataRoute!
    var routeID: Int?
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD.show(.progress)
        Network.request(target: .getRouteWithID(id: self.routeID!))
            .mapObject(dataRoute.self)
            .subscribe(onNext: { (route) in
                self.competition = route
                let calculator = IndicatorsCalculator()

                self.txtDistance.text = stringKM(calculator._distance(inputDistance: self.competition.distance))
                let bestTime = self.competition.records.reduce(0) {$0 + $1.duration_seconds}
                self.txtTime.text = "\(bestTime)"
                if self.competition.records.count > 0 {
                    let bestRecord = self.competition.records.max{ $0.duration_seconds > $1.duration_seconds }
                    self.txtSport.text = "\(bestRecord!.sport.title)"
                    self.txtTime.text = stringFromTimeInterval(sec: bestRecord!.duration_seconds)
                }
                let locations = self.competition.route.map {
                    return CLLocationCoordinate2D(latitude: $0.lat, longitude: $0.lon)
                }
                self.map.addPolygon(points: locations)
                }, onError: { (error) in
                    HUD.hide()
                }, onCompleted: {
                    HUD.hide()
                }, onDisposed: { 
            }).addDisposableTo(bag)
        
        followBtn.rx.tap.bindNext {
            let followVC = st.instantiateViewController(withIdentifier: "training") as! TrainingVC
            followVC.routeID = self.routeID
            followVC.route = self.competition.route.map {
                let location = CLLocationCoordinate2D(latitude: $0.lat, longitude: $0.lon)
                return location
            }
            self.navigationController?.pushViewController(followVC, animated: true)
            }.addDisposableTo(bag)
        
        recordsBtn.rx.tap.bindNext {
            HUD.show(.progress)
            Network.request(target: .getTrainingWithID(id: self.routeID!))
                .mapObject(DataTR.self)
                .subscribe(onNext: { (training) in
                    HUD.show(.progress)
                    Network.request(target: .getAllRecordsForRoute(id: self.routeID!))
                        .mapObject(RecordsArr.self)
                        .subscribe(onNext: { (records) in
                            let recordsVC = st.instantiateViewController(withIdentifier: "records") as! ResultsTC
                            recordsVC.data = records.data ?? []
                            self.navigationController?.pushViewController(recordsVC, animated: true)
                            }, onError: { (error) in
                                HUD.hide()
                                showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                            }, onCompleted: {
                                HUD.hide()
                            }, onDisposed: nil).addDisposableTo(self.rx_disposeBag)
                    }, onError: { (error) in
                        HUD.hide()
                        showErrorAlertWithCode(code: error.localizedDescription, controller: self)
                    }, onCompleted: {
                        HUD.hide()
                    }, onDisposed: nil).addDisposableTo(self.rx_disposeBag)
        }.addDisposableTo(rx_disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func mapView(_ mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        return map.lineView(rendererForOverlay: overlay)
    }
}
