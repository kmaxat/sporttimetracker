//
//  LeftMenu.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 08.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RealmSwift
import IBAnimatable
import Nuke
import RxSwift

class LeftMenu: UITableViewController {
    
    @IBOutlet weak var avatar: AnimatableImageView!
    @IBOutlet fileprivate weak var txtName: UILabel!
    @IBOutlet weak var messagesCount: UILabel!
    var profileVC: UIViewController!
    var trainingVC: UIViewController!
    var grupTrainingsVC: UIViewController!
    var friendsVC: UIViewController!
    var hystoryVC: UIViewController!
    var competitionsVC: UIViewController!
    var inboxVC: UIViewController!
    var settingsVC: UIViewController!
    
    var profileVCSW: UIViewController!
    var trainingVCSW: UIViewController!
    var grupTrainingsVCSW: UIViewController!
    var friendsVCSW: UIViewController!
    var hystoryVCSW: UIViewController!
    var competitionsVCSW: UIViewController!
    var inboxVCSW: UIViewController!
    var settingsVCSW: UIViewController!
    
    var mainViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.trainingVC = st.instantiateViewController(withIdentifier: "training") as! TrainingVC
        self.trainingVCSW = UINavigationController(rootViewController: trainingVC)
        
        self.profileVC = st.instantiateViewController(withIdentifier: "profile") as! ProfileTC
        self.profileVCSW = UINavigationController(rootViewController: profileVC)
        
        self.grupTrainingsVC = st.instantiateViewController(withIdentifier: "group") as! GroupTrainingsVC
        self.grupTrainingsVCSW = UINavigationController(rootViewController: grupTrainingsVC)
        
        self.friendsVC = st.instantiateViewController(withIdentifier: "friends") as! FriendsVC
        self.friendsVCSW = UINavigationController(rootViewController: friendsVC)
        
        self.hystoryVC = st.instantiateViewController(withIdentifier: "hystory") as! HystoryTC
        self.hystoryVCSW = UINavigationController(rootViewController: hystoryVC)
        
        self.competitionsVC = st.instantiateViewController(withIdentifier: "competition") as! CompetitionsVC
        self.competitionsVCSW = UINavigationController(rootViewController: competitionsVC)
        
        self.inboxVC = st.instantiateViewController(withIdentifier: "inbox") as! InboxVC
        self.inboxVCSW = UINavigationController(rootViewController: inboxVC)
        
        self.settingsVC = st.instantiateViewController(withIdentifier: "settings") as! SettingsTC
        self.settingsVCSW = UINavigationController(rootViewController: settingsVC)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Network.request(target: .getUserMe())
        .mapObject(User.self)
        .subscribe(onNext: { (user) in
            self.txtName.text = user.first_name + " " + user.last_name
            if let url = URL(string: user.picture ?? "") {
                Nuke.loadImage(with: url, into: self.avatar)
            }
            }, onError: nil, onCompleted: nil, onDisposed: nil).addDisposableTo(rx_disposeBag)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedVC: UIViewController!
        switch (indexPath as NSIndexPath).row {
        case 0:
            selectedVC = profileVCSW
            break
        case 1:
            selectedVC = trainingVCSW
            break
        case 2:
            selectedVC = grupTrainingsVCSW
            break
        case 3:
            selectedVC = friendsVCSW
            break
        case 4:
            selectedVC = hystoryVCSW
            break
        case 5:
            selectedVC = competitionsVCSW
            break
        case 6:
            selectedVC = inboxVCSW
            break
        case 7:
            selectedVC = settingsVCSW
        default:
            break
        }
        self.slideMenuController()?.changeMainViewController(selectedVC, close: true)
    }

}
