//
//  Token.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 12.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import ObjectMapper

class Token: Mappable {

    var token: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        token <- map["token"]
    }
}
