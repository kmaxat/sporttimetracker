//
//  ChooseSportTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 16.06.16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import RxSwift
import Nuke
import PKHUD

protocol ChooseSportDelegate {
    func didiChooseSport(_ sport: dataSport)
}

class ChooseSportTC: UITableViewController {
    
    var data = [dataSport]()
    var viewModel: CreateTrainingViewModel!
    let disposeBag = DisposeBag()
    var delegate: ChooseSportDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
    }
    
    func getData() {
        HUD.show(.progress)
        Network.request(target: .getSports())
            .mapObject(Sport.self)
            .subscribe(onNext: { (sport) in
                self.data = sport.data ?? []
                self.tableView.reloadData()
                }, onError: { (error) in
                    HUD.hide()
                }, onCompleted: {
                    HUD.hide()
                }, onDisposed: nil).addDisposableTo(disposeBag)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                                               for: indexPath) as! SportCell
        let sport = data[indexPath.row]
        cell.txt.text = sport.title
        cell.img.image = nil
        Nuke.loadImage(with: URL(string: sport.picture)!, into: cell.img)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didiChooseSport(data[(indexPath as NSIndexPath).row])
        _ = self.navigationController?.popViewController(animated: true)
    }
}
