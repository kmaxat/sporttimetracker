//
//  UsersCountsTC.swift
//  SportTimeTracker
//
//  Created by vadim vitvickiy on 22/08/16.
//  Copyright © 2016 vadim vitvickiy. All rights reserved.
//

import UIKit
import PKHUD
import RxSwift
import Nuke

class UsersCountsTC: UITableViewController {
    
    var data = [DataFriends]()
    var trainingID: Int!
    var type: String!
    
    let bag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(self.getData), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
    }
    
    func getData() {
        HUD.show(.progress)
        Network.request(target: .groupTrainingUsersList(id: trainingID, type: type))
            .mapObject(Friends.self)
            .subscribe(onNext: { (friends) in
                self.data = friends.data ?? []
                self.tableView.reloadData()
                }, onError: { (error) in
                    HUD.hide()
                    self.refreshControl?.endRefreshing()
                }, onCompleted: { 
                    HUD.hide()
                    self.refreshControl?.endRefreshing()
                }, onDisposed: nil).addDisposableTo(bag)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                                               for: indexPath) as! UserCell
        let user = data[(indexPath as NSIndexPath).row]
        cell.txt.text = user.first_name + " " + user.last_name
        cell.img.image = nil
        if let url = URL(string: user.picture ?? "") {
            Nuke.loadImage(with: url, into: cell.img)
        }else {
            cell.img.image = #imageLiteral(resourceName: "avatar_placeholder")
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profileVC = st.instantiateViewController(withIdentifier: "User") as! UserProfileVC
        profileVC.userID = self.data[(indexPath as NSIndexPath).row].id
        profileVC.hideCloseBtn = true
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
